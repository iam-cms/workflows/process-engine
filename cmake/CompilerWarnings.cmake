function(target_clang_compile_warnings target scope)
  if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    return()
  endif()

  target_compile_options(
    ${target}
    ${scope}
    -Wall
    -Wextra
    -Wshadow
    -Wnon-virtual-dtor
    -pedantic
    -fdiagnostics-color=always)

endfunction()

function(target_gcc_compile_warnings target scope)
  if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    return()
  endif()

  target_compile_options(
    ${target}
    ${scope}
    -Wall
    -Wextra
    -Wshadow
    -Wnon-virtual-dtor
    -pedantic
    -fdiagnostics-color=always)

endfunction()

function(target_msvc_compile_warnings target scope)
  if(NOT CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    return()
  endif()

  target_compile_options(
    ${target}
    ${scope}
    /permissive
    /W4
    /w14640)

endfunction()

function(target_compile_warnings target scope)
  target_clang_compile_warnings(${target} ${scope})
  target_gcc_compile_warnings(${target} ${scope})

endfunction()