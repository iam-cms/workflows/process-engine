# process engine
This is a workflow execution engine to execute kadi workflows.

## Download and install latest .deb package
```
wget https://gitlab.com/iam-cms/workflows/process-engine/-/jobs/artifacts/master/download?job=pack_deb -O process-engine.zip
unzip process-engine.zip
sudo apt install ./build/process-engine_[VERSION]_[SYSTEM ARCHITECTURE].deb
```

## How to build
```
mkdir build
cmake -S . -B build
cmake --build build/
```

## Usage
Running a workflow:
```
process-engine start -p /tmp/my-execution-folder my-workflow.flow
```
More info can be obtained with `process-engine --help` and `process-engine [sub-command] --help`.

## Configuration for the process manager
The API which the [process manager](https://gitlab.com/iam-cms/workflows/process-manager) expects was configurable in the past, but now the only configuration is "name" and "path" for each process engine.

# Development

To also build the unit tests, set the cmake variable `build_test` to `ON` when generating the cmake cache (default is `OFF`):
```
cmake .. -Dbuild_tests=ON
```
