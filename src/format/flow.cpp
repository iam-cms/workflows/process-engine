#include "flow.hpp"

#include "node/control.hpp"
#include "node/file_io.hpp"
#include "node/misc.hpp"
#include "node/source.hpp"
#include "node/subworkflow.hpp"
#include "node/tool.hpp"
#include "node/user_input.hpp"
#include "node/user_output.hpp"

using namespace process_engine::node;
using namespace process_engine::workflow;

namespace process_engine::flow {
  std::pair<std::vector<Port>, std::vector<Port>> extract_input_and_output_ports(const nlohmann::json &ports_json) noexcept {
    const std::unordered_map<std::string_view, std::string_view> type_mappings{
      {"flag", type_to_string<bool>()},
      {"real", type_to_string<float>()},
      {"long", type_to_string<int>()},
      {"choice", type_to_string<std::string>()},
    };

    std::vector<Port> inputs{};
    std::vector<Port> outputs{};

    int positional{};

    for (const auto &port : ports_json) {
      const std::string dir = port["port_direction"];
      const std::string name = port["name"];
      const std::string short_name = port["shortName"];
      std::string type = port["type"];

      if (type_mappings.contains(type)) {
        type = type_mappings.at(type);
      }

      auto &node_ports = dir == "in" ? inputs : outputs;

      if (type == type_to_string<Dependency>()) {
        node_ports.emplace_back(DependenciesPort::create());
      } else if (type == type_to_string<Env>()) {
        node_ports.emplace_back(EnvPort::create());
      } else if (type == "pipe") {
        node_ports.emplace_back(Port{name, std::string(type_to_string<std::string>()), {}, {}});
      } else {
        if (type == "flag") {
          type = std::string(type_to_string<bool>());
        }

        auto &added_port = node_ports.emplace_back(Port{name, type, {}, {}});

        if (port.contains("positional")) {
          added_port.hints["inputBinding"]["position"] = positional++;
        } else if (!short_name.empty()) {
          added_port.hints["inputBinding"]["prefix"] = "-" + short_name;
        } else {
          added_port.hints["inputBinding"]["prefix"] = "--" + name;
        }
      }
    }

    return {inputs, outputs};
  }

  std::unordered_map<std::string, std::string> extract_variables(const nlohmann::json &variables_json) noexcept {
    std::unordered_map<std::string, std::string> variables{};

    for (const auto &variable : variables_json) {
      variables.insert({variable["name"].get<std::string>(), variable["value"].get<std::string>()});
    }

    return variables;
  }

  std::unique_ptr<Node> extract_node(const nlohmann::json &node_json) noexcept {
    const std::string &name = node_json["model"]["name"];
    std::string id = node_json["id"];

    if (name == StringSource::name) {
      return create_string_source(std::move(id), node_json["model"]["value"]);
    } else if (name == IntSource::name) {
      return create_int_source(std::move(id), node_json["model"]["value"]);
    } else if (name == FloatSource::name) {
      return create_float_source(std::move(id), node_json["model"]["value"]);
    } else if (name == BoolSource::name) {
      return create_bool_source(std::move(id), node_json["model"]["value"]);
    }

    if (name == UserInputText::name) {
      return create_user_input_text(std::move(id));
    } else if (name == UserInputBool::name) {
      return create_user_input_bool(std::move(id));
    } else if (name == UserInputFloat::name) {
      return create_user_input_float(std::move(id));
    } else if (name == UserInputInteger::name) {
      return create_user_input_integer(std::move(id));
    } else if (name == UserInputSelect::name) {
      return create_user_input_select(std::move(id));
    } else if (name == UserInputPeriodicTable::name) {
      return create_user_input_periodic_table(std::move(id));
    } else if (name == UserInputChoose::name) {
      return create_user_input_choose(std::move(id), node_json["model"]["nOptions"]);
    } else if (name == UserInputFile::name) {
      return create_user_input_file(std::move(id));
    } else if (name == UserInputForm::name) {
      return create_user_input_form(std::move(id));
    } else if (name == UserInputSelectBoundingBox::name) {
      return create_user_input_select_bounding_box(std::move(id));
    }

    if (name == UserOutputText::name) {
      return create_user_output_text(std::move(id));
    } else if (name == UserOutputWebView::name) {
      return create_user_output_web_view(std::move(id));
    }

    if (name == IfBranch::name) {
      return create_if_branch(std::move(id));
    } else if (name == Loop::name) {
      return create_loop(std::move(id));
    } else if (name == Variable::name) {
      return create_variable(std::move(id));
    } else if (name == VariableList::name) {
      return create_variable_list(std::move(id));
    } else if (name == BranchSelectNode::name) {
      return create_branch_select(std::move(id), node_json["model"]["nBranches"]);
    }

    if (name == FileInputNode::name) {
      return create_file_input(std::move(id));
    } else if (name == FileOutputNode::name) {
      return create_file_output(std::move(id), node_json["model"].contains("createShortcut") ? node_json["model"]["createShortcut"].get<bool>() : false);
    }

    if (name == ToolNode::name) {
      const auto &model_json = node_json["model"];
      const auto &tool_json = model_json["tool"];

      auto [inputs, outputs] = extract_input_and_output_ports(tool_json["ports"]);
      const auto tool_name = tool_json["name"];
      const auto tool_path = tool_json["path"];
      const auto run_detached = model_json["executionProfile"].get<std::string>() == "Detached";
      const auto skip = model_json["executionProfile"].get<std::string>() == "Skip";

      auto tool_node = create_tool(std::move(id), std::move(inputs), std::move(outputs), tool_name, tool_path, run_detached);
      tool_node->skip = skip;

      return tool_node;
    } else if (name == EnvNode::name) {
      return create_env(std::move(id), node_json["model"]["tool"]["path"]);
    }

    if (name == FormatStringNode::name) {
      return create_format_string(std::move(id), node_json["model"]["nInputs"], node_json["model"]["value"]);
    }

    if (name == WorkflowNode::name) {
      return create_workflow(std::move(id), node_json["model"]["workflow"]["path"]);
    }

    return {};
  }

  std::optional<Nodes> extract_all_nodes(const nlohmann::json &nodes_json) noexcept {
    Nodes nodes{};

    for (const auto &node_json : nodes_json) {
      if (node_json["model"]["name"] == "Note") {
        continue;
      }

      auto node = extract_node(node_json);
      if (node == nullptr) {
        return std::nullopt;
      }

      nodes.push_back(std::move(node));
    }

    return nodes;
  }

  Graph extract_graph(const Nodes &nodes, const nlohmann::json &connections_json) noexcept {
    Graph graph{};

    // Insert every node in the graph, regardless if they have a connection or not.
    for (const auto &node : nodes) {
      graph.insert({node.get(), {}});
    }

    const auto find_node_by_id = [&nodes](const std::string &id) {
      return std::find_if(nodes.begin(), nodes.end(), [&id](const auto &node) {
        return node->id == id;
      });
    };

    // Connect the single nodes together.
    for (const auto &connection : connections_json) {
      const auto src_node = find_node_by_id(connection["out_id"])->get();
      const auto dst_node = find_node_by_id(connection["in_id"])->get();

      // Create a edge in both directions for easier graph traversal.
      graph.at(src_node).outgoing.insert({connection["out_index"], {dst_node, connection["in_index"]}});
      graph.at(dst_node).incoming.insert({connection["in_index"], {src_node, connection["out_index"]}});
    }

    return graph;
  }

  std::optional<workflow::Workflow> from_memory(const nlohmann::json &flow_json) noexcept {
    auto nodes = extract_all_nodes(flow_json["nodes"]);
    if (!nodes) {
      return std::nullopt;
    }

    auto graph = flow::extract_graph(nodes.value(), flow_json["connections"]);
    auto variables = flow::extract_variables(flow_json["variables"]);

    if (!validate(graph).empty()) {
      std::cerr << "Graph is invalid\n";
      return std::nullopt;
    }

    return Workflow{
      .nodes = std::move(nodes.value()),
      .graph = std::move(graph),
      .variables = std::move(variables),
      .interactions = {},
    };
  }

  std::optional<workflow::Workflow> from_file(const std::filesystem::path &flow_file) noexcept {
    const auto json = utility::load_json(flow_file);
    if (!json) {
      return {};
    }

    return from_memory(json.value());
  }

  bool is_compatible(const std::filesystem::path &path) noexcept {
    return path.extension() == ".flow";
  }
} // namespace process_engine::flow