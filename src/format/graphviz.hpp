#pragma once

#include <sstream>
#include <string>
#include <string_view>

namespace process_engine::graphviz {
  static constexpr std::string_view SHAPE_OVAL{"oval"};
  static constexpr std::string_view SHAPE_DIAMOND{"diamond"};
  static constexpr std::string_view SHAPE_RECT{"rect"};

  static constexpr std::string_view GRAPH_DIR_TOP_TO_BOTTOM{"TB"};
  static constexpr std::string_view GRAPH_DIR_BOTTOM_TO_TOP{"BT"};
  static constexpr std::string_view GRAPH_DIR_LEFT_TO_RIGHT{"LR"};
  static constexpr std::string_view GRAPH_DIR_RIGHT_TO_LEFT{"RL"};

  static constexpr std::string_view PORT_TOP{"n"};
  static constexpr std::string_view PORT_RIGHT{"e"};
  static constexpr std::string_view PORT_BOTTOM{"s"};
  static constexpr std::string_view PORT_LEFT{"w"};

  struct NodeOptions final {
    std::string_view label{};
    std::string_view shape{};
    std::string_view image{};
  };

  struct ConnectionOptions final {
    std::string_view label{};
    std::string_view from_port{};
    std::string_view to_port{};
  };

  [[nodiscard]] std::string begin_digraph(std::string_view graph_name, std::string_view dir = GRAPH_DIR_TOP_TO_BOTTOM) noexcept;
  [[nodiscard]] std::string end() noexcept;

  [[nodiscard]] std::string node(std::string_view name, const NodeOptions &options) noexcept;
  [[nodiscard]] std::string connection(std::string_view from, std::string_view to, const ConnectionOptions &options) noexcept;

} // namespace process_engine::graphviz