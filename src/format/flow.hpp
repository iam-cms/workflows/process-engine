#pragma once

#include "node/node.hpp"
#include "node/port.hpp"
#include "workflow/graph.hpp"
#include "workflow/workflow.hpp"

#include <nlohmann/json.hpp>

#include <string>
#include <unordered_map>
#include <vector>

namespace process_engine::flow {
  [[nodiscard]] std::pair<std::vector<node::Port>, std::vector<node::Port>> extract_input_and_output_ports(const nlohmann::json &ports_json) noexcept;
  [[nodiscard]] std::unordered_map<std::string, std::string> extract_variables(const nlohmann::json &variables_json) noexcept;
  [[nodiscard]] std::unique_ptr<node::Node> extract_node(const nlohmann::json &node_json) noexcept;
  [[nodiscard]] std::optional<workflow::Nodes> extract_all_nodes(const nlohmann::json &nodes_json) noexcept;
  [[nodiscard]] workflow::Graph extract_graph(const workflow::Nodes &nodes, const nlohmann::json &connections_json) noexcept;

  std::optional<workflow::Workflow> from_memory(const nlohmann::json &flow_json) noexcept;
  std::optional<workflow::Workflow> from_file(const std::filesystem::path &flow_file) noexcept;

  [[nodiscard]] bool is_compatible(const std::filesystem::path &path) noexcept;
} // namespace process_engine::flow