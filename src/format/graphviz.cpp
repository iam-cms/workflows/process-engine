#include "graphviz.hpp"

#include <sstream>

namespace process_engine::graphviz {

  static constexpr std::string_view INDENT{"    "};

  std::string begin_digraph(std::string_view graph_name, std::string_view dir) noexcept {
    std::stringstream result{};
    result << "digraph " << graph_name << "\n{\n";

    result << INDENT << "splines=spline\n";

    result << INDENT << "rankdir=\"" << dir << "\"\n";
    result << INDENT << "edge [arrowhead=none]\n";
    result << INDENT << "concentrate=true\n";

    result << '\n';

    return result.str();
  }

  std::string end() noexcept {
    return "}\n";
  }

  std::string node(std::string_view name, const NodeOptions &options) noexcept {
    std::stringstream result{};

    result << INDENT << name << "\n";
    result << INDENT << "[\n";

    if (options.image.empty()) {
      result << INDENT << INDENT << "shape = " << options.shape << "\n";
      result << INDENT << INDENT << "label = \"" << options.label << "\"\n";
    } else {
      result << INDENT << INDENT << "shape = circle\n";
      result << INDENT << INDENT << "label = \"\"\n";
      result << INDENT << INDENT << "image = \"" << options.image << "\"\n";
      result << INDENT << INDENT << "color = \"#00000000\"\n";
      result << INDENT << INDENT << "imagescale = true\n";
      result << INDENT << INDENT << "fixedsize = true\n";
    }

    result << INDENT << "]\n\n";

    return result.str();
  }

  std::string connection(std::string_view from, std::string_view to, const ConnectionOptions &options) noexcept {
    std::stringstream result{};

    result << INDENT;
    result << from;

    if (!options.from_port.empty()) {
      result << ":" << options.from_port;
    }

    result << " -> ";
    result << to;

    if (!options.to_port.empty()) {
      result << ":" << options.to_port;
    }

    result << " [";

    if (!options.label.empty()) {
      result << "label = \"" << options.label << "\" ";
    }

    result << "]\n";

    return result.str();
  }

} // namespace process_engine::graphviz