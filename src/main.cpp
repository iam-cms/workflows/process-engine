/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "cli/common.hpp"
#include "cli/control.hpp"
#include "cli/graphviz.hpp"
#include "cli/interaction.hpp"
#include "cli/state.hpp"
#include "cli/tools.hpp"
#include "logging.hpp"
#include "workflow/io.hpp"
#include "workflow/workflow.hpp"

#ifdef WIN32
#include <winsock2.h>
#endif

#include <iostream>

using namespace process_engine;

int main(int argc, char *argv[]) {
  CLI::App app{"Process Engine: Executes Kadi workflows"};
  app.set_version_flag("--version", PROCESS_ENGINE_VERSION);
  app.require_subcommand(1);

  cli::CommonOptions common_options{};
  cli::RunOptions run_options{};

  const auto run_cmd = app.add_subcommand("run")->alias("start");
  run_cmd->add_flag("-C,--no-color", run_options.no_color, "Disable terminal colors");
  run_cmd->add_flag("-e,--exit", run_options.exit, "Terminates the process when nothing more can be processed");
  run_cmd->add_option("file", run_options.workflow_file, "Workflow description file (.flow)")->required(true)->transform(cli::path_validator);
  cli::add_common_options(run_cmd, common_options);

  const auto continue_cmd = app.add_subcommand("continue", "Continue workflow execution after user interaction");
  cli::add_common_options(continue_cmd, common_options);

  const auto stop_cmd = app.add_subcommand("stop", "Stops the workflow after the currently executed node has been completed");
  cli::add_common_options(stop_cmd, common_options);

  const auto cancel_cmd = app.add_subcommand("cancel", "Cancel workflow execution");
  cli::add_common_options(cancel_cmd, common_options);

  const auto interactions_cmd = app.add_subcommand("interactions", "Print interaction descriptions");
  cli::add_common_options(interactions_cmd, common_options);

  cli::InputOptions input_options{};
  const auto input_cmd = app.add_subcommand("input", "Store an iput value for given interaction");
  input_cmd->add_option("interactionID", input_options.interaction_id, "ID of the interaction")->required();
  input_cmd->add_option("value", input_options.value, "Value")->required();
  cli::add_common_options(input_cmd, common_options);

  const auto status_cmd = app.add_subcommand("status", "Print the status of a workflow execution");
  cli::add_common_options(status_cmd, common_options);

  const auto artifacts_cmd = app.add_subcommand("artifacts")->alias("shortcuts");
  cli::add_common_options(artifacts_cmd, common_options);

  const auto log_cmd = app.add_subcommand("log", "Print the status of a workflow execution");
  cli::add_common_options(log_cmd, common_options);

  const auto log_path_cmd = app.add_subcommand("log_path", "Print the absolute path to the log file for an existing workflow execution");
  cli::add_common_options(log_path_cmd, common_options);


  const auto tree_path_cmd = app.add_subcommand("tree_path", "Print the absolute path to the file containing the status tree information for an existing workflow execution");
  cli::add_common_options(tree_path_cmd, common_options);

  try {
    app.parse(argc, argv);
  } catch (const CLI::ParseError &e) {
    return app.exit(e);
  }

  logging::setup_execution_logging(workflow::execution_log_path(common_options.working_dir), !run_options.no_color);

  int result{};

#ifdef WIN32
  WSADATA win32_wsa_data;
  result = WSAStartup(MAKEWORD(2, 2), &win32_wsa_data);

  if (result != 0) {
    LOG_ENGINE_EXECUTION_ERROR(logging::execution_logger(), "Could not startup winsock");
    return result;
  }
#endif

  try {
    if (*run_cmd) {
      result = cli::run_workflow(common_options.working_dir, run_options);
    } else if (*continue_cmd) {
      result = cli::continue_workflow(common_options.working_dir, argv[0]);
    } else if (*stop_cmd) {
      result = cli::stop_workflow(common_options.working_dir);
    } else if (*cancel_cmd) {
      result = cli::cancel_workflow(common_options.working_dir);
    } else if (*status_cmd) {
      result = cli::workflow_status(common_options.working_dir);
    } else if (*log_cmd) {
      result = cli::show_log(common_options.working_dir);
    } else if (*log_path_cmd) {
      result = cli::print_log_path(common_options.working_dir);
    } else if (*interactions_cmd) {
      result = cli::list_interactions(common_options.working_dir);
    } else if (*input_cmd) {
      result = cli::add_input(common_options.working_dir, input_options);
    } else if (*artifacts_cmd) {
      result = cli::list_artifacts(common_options.working_dir);
    } else if (*tree_path_cmd) {
      result = cli::print_tree_path(common_options.working_dir);
    }
  } catch (const std::exception &e) {
    LOG_ENGINE_EXECUTION_INFO(logging::execution_logger(), e.what());
    result = 1;
  }

#if WIN32
  WSACleanup();
#endif

  return result;
}
