#include "io.hpp"

#include "format/flow.hpp"
#include "utility.hpp"

namespace {
  template<typename T>
  std::optional<T> load_from_file(const std::filesystem::path &path) {
    const auto content = process_engine::utility::read_file_as_string(path);
    if (!content) {
      return {};
    }

    if (!nlohmann::json::accept(content.value())) {
      return {};
    }

    return nlohmann::json::parse(content.value()).get<T>();
  }
} // namespace

namespace process_engine::workflow {
  std::optional<Workflow> from_file(const std::filesystem::path &file) noexcept {
    std::optional<Workflow> workflow{};

    if (file.extension() == ".flow") {
      workflow = flow::from_file(file);
    }

    if (workflow) {
      workflow->file = std::filesystem::canonical(file);
    }

    return workflow;
  }

  std::filesystem::path log_dir(const std::filesystem::path &working_dir) noexcept {
    return working_dir / "logs";
  }

  std::filesystem::path execution_log_path(const std::filesystem::path &working_dir) noexcept {
    return log_dir(working_dir) / "execution.txt";
  }

  std::filesystem::path ipc_dir(const std::filesystem::path &working_dir) noexcept {
    return working_dir / "ipc";
  }

  std::filesystem::path ipc_socket_path(const std::filesystem::path &working_dir) noexcept {
    return ipc_dir(working_dir) / "process_engine.sock";
  }

  std::filesystem::path tmp_dir(const std::filesystem::path &working_dir) noexcept {
    return working_dir / "tmp";
  }

  std::filesystem::path state_dir(const std::filesystem::path &working_dir) noexcept {
    return working_dir / "state";
  }

  std::filesystem::path interactions_path(const std::filesystem::path &working_dir) noexcept {
    return state_dir(working_dir) / "interactions.json";
  }

  std::filesystem::path nodes_path(const std::filesystem::path &working_dir) noexcept {
    return state_dir(working_dir) / "nodes.json";
  }

  std::filesystem::path status_path(const std::filesystem::path &working_dir) noexcept {
    return state_dir(working_dir) / "status.json";
  }

  bool load_node_states_from_file(Nodes &nodes, const std::filesystem::path &working_dir) noexcept {
    const auto content = utility::read_file_as_string(nodes_path(working_dir));
    if (!content) {
      return false;
    }

    if (!nlohmann::json::accept(content.value())) {
      return false;
    }


    const auto find_node_by_id = [&nodes](const std::string &id) {
      return std::find_if(nodes.begin(), nodes.end(), [&id](const auto &node) {
        return node->id == id;
      });
    };

    const auto json = nlohmann::json::parse(content.value());

    for (const auto &node_state : json) {
      const std::string id = node_state["id"];

      auto node = find_node_by_id(id)->get();
      node->state = node_state["state"];
      node->token = node_state["token"];
      node->inputs = node_state["inputs"].get<std::vector<node::Port>>();
      node->outputs = node_state["outputs"].get<std::vector<node::Port>>();
    }

    return true;
  }

  bool store_node_states_to_file(const Nodes &nodes, const std::filesystem::path &working_dir) noexcept {
    try {
      return utility::save_file(nodes_path(working_dir), nlohmann::json(nodes).dump(2));
    } catch ([[maybe_unused]] const std::exception &e) {
      return false;
    }
  }

  std::optional<Interactions> load_interactions_from_file(const std::filesystem::path &working_dir) noexcept {
    return load_from_file<Interactions>(interactions_path(working_dir));
  }

  bool store_interactions_to_file(const Interactions &interactions, const std::filesystem::path &working_dir) noexcept {
    try {
      return utility::save_file(interactions_path(working_dir), nlohmann::json(interactions).dump(2));
    } catch ([[maybe_unused]] const std::exception &e) {
      return false;
    }
  }

  bool load_status_from_file(Workflow &workflow, const std::filesystem::path &working_dir) noexcept {
    const auto json = utility::load_json(status_path(working_dir));
    if (!json) {
      return false;
    }

    json->at("started_at").get_to(workflow.started_at);
    json->at("finished_at").get_to(workflow.finished_at);
    json->at("nodes_finished").get_to(workflow.nodes_finished);
    json->at("variables").get_to(workflow.variables);
    json->at("artifacts").get_to(workflow.artifacts);
    json->at("state").get_to(workflow.state);

    return true;
  }

  bool store_status_to_file(const Workflow &workflow, const std::filesystem::path &working_dir) noexcept {
    nlohmann::json j{
      {"started_at", workflow.started_at},
      {"finished_at", workflow.finished_at},
      {"nodes_finished", workflow.nodes_finished},
      {"variables", workflow.variables},
      {"artifacts", workflow.artifacts},
      {"state", workflow.state},
    };

    return utility::save_file(status_path(working_dir), j.dump(2));
  }

  std::optional<std::filesystem::path> find_workflow_file(const std::filesystem::path &working_dir) noexcept {
    for (const auto &dir_entry : std::filesystem::directory_iterator(working_dir)) {
      if (flow::is_compatible(dir_entry.path())) {
        std::error_code error{};
        const auto absolute_path = std::filesystem::canonical(dir_entry.path(), error);
        return error ? std::nullopt : std::optional<std::filesystem::path>{absolute_path};
      }
    }

    return std::nullopt;
  }
} // namespace process_engine::workflow