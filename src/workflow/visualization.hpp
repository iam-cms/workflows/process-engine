#pragma once

#include "workflow/workflow.hpp"

#include <string>

namespace process_engine::workflow {
  [[nodiscard]] std::string to_graphviz(const Graph &graph, const std::vector<std::pair<std::string, std::string>> &icons) noexcept;
} // namespace process_engine::workflow