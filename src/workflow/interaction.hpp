#pragma once

#include <nlohmann/json.hpp>

#include <optional>
#include <string>
#include <vector>

namespace process_engine::workflow {
  struct Interaction final {
    // TODO Useful?
    enum class Source {
      user,
      variable,
    };

    enum class Direction {
      input,
      output,
    };

    std::string id{};
    std::string node_id{};
    Source source{};
    std::string type{};
    std::string description{};
    nlohmann::json default_value{};
    nlohmann::json value{};

    nlohmann::json options{};
    nlohmann::json url{};
    nlohmann::json multiline{};
    Direction direction{};

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(Interaction, id, node_id, type, description, default_value, value, options, url, multiline, direction)
  };

  using Interactions = std::vector<Interaction>;

  [[nodiscard]] Interaction *find_interaction(const std::string &node_id, Interactions &interactions) noexcept;
  [[nodiscard]] Interaction *find_or_add_interaction(const std::string &node_id, Interactions &interactions) noexcept;

  NLOHMANN_JSON_SERIALIZE_ENUM(Interaction::Source, {{Interaction::Source::user, "user"}, {Interaction::Source::variable, "variable"}})
  NLOHMANN_JSON_SERIALIZE_ENUM(Interaction::Direction, {{Interaction::Direction::input, "input"}, {Interaction::Direction::output, "output"}})
} // namespace process_engine::workflow