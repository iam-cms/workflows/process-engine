/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <algorithm>
#include <map>

namespace process_engine::graph {

  /// @brief Edge from one node into the port of another node.
  template<typename Node>
  struct Edge final {
    Node node{};
    size_t port_index{};
  };

  /// @brief All incoming and outgoing connections of a node.
  /// @tparam Type of the node.
  template<typename Node>
  struct Ports final {
    std::multimap<size_t, Edge<Node>> outgoing{};
    std::multimap<size_t, Edge<Node>> incoming{};
  };

  /// @brief Describes a graph as adjacency list.
  /// @tparam Type of the node.
  template<typename Node>
  using AdjacencyList = std::unordered_map<Node, Ports<Node>>;

  template<typename Node>
  AdjacencyList<Node> &remove_dangling_edges(AdjacencyList<Node> &graph) noexcept {
    const auto clean_ports = [&graph](auto &ports) {
      for (auto it = ports.begin(); it != ports.end();) {
        if (!graph.contains(it->second.node)) {
          it = ports.erase(it);
        } else {
          ++it;
        }
      }
    };

    for (auto &[node, ports] : graph) {
      clean_ports(ports.incoming);
      clean_ports(ports.outgoing);
    }

    return graph;
  }

  template<typename Node>
  AdjacencyList<Node> &exclude_node(AdjacencyList<Node> &graph, const Node node) noexcept {
    if (!graph.contains(node)) {
      return graph;
    }

    auto &ports = graph.at(node);

    for (auto &[dst_port_index, incoming_edge] : ports.incoming) {
      auto previous_node = incoming_edge.node;

      // Connect the previous nodes to all subsequent nodes because of transitive dependencies.
      for (auto &[src_port_index, outgoing_edge] : ports.outgoing) {
        auto next_node = outgoing_edge.node;

        graph.at(previous_node).outgoing.insert({0, {next_node, 0}});
        graph.at(next_node).incoming.insert({0, {previous_node, 0}});
      }
    }

    graph.erase(node);
    return remove_dangling_edges(graph);
  }

  /// @brief Try to determine the basic order of the nodes.
  /// @tparam Node Type of the node.
  /// @param graph The graph.
  /// @return Sorted nodes or std::nullopt when the graph contains cycles.
  template<typename Node>
  [[nodiscard]] std::optional<std::vector<Node>> sort_topological(const AdjacencyList<Node> &graph) noexcept {
    std::unordered_map<Node, int> dependencies{};

    std::vector<Node> result{};

    // Initialize the input dependency count for every node.
    for (const auto &[node, ports] : graph) {
      dependencies.insert({node, ports.incoming.size()});
    }

    while (!dependencies.empty()) {
      // Try to find a node with zero input dependencies.
      const auto find_iter = std::find_if(dependencies.begin(), dependencies.end(), [](const auto &pair) {
        return pair.second == 0;
      });

      // If no node with zero input dependencies was found, the graph has cycles.
      if (find_iter == dependencies.end()) {
        return std::nullopt;
      }

      // Reduce the input dependency of the connected nodes.
      for (auto &[port_index, dst] : graph.at(find_iter->first).outgoing) {
        if (dependencies.contains(dst.node)) {
          --dependencies.at(dst.node);
        }
      }

      result.push_back(find_iter->first);
      dependencies.erase(find_iter);
    }

    return result;
  }
} // namespace process_engine::graph