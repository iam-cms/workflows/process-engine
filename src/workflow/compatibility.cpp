#include "compatibility.hpp"

#include "node/source.hpp"
#include "utility.hpp"
#include "workflow/io.hpp"

namespace process_engine::workflow {
  void make_interaction_compatible(nlohmann::json &interaction_json, int order) noexcept {
    interaction_json["order"] = order;
    interaction_json["pageNumber"] = 0;
  }


  nlohmann::json build_tree(const Graph &graph) noexcept {
    const auto order = graph::sort_topological(graph);
    if (!order) {
      return {};
    }

    nlohmann::json j{};
    int index{};

    for (const auto node : order.value()) {
      if (node::is_source_node(node->name)) {
        continue;
      }

      nlohmann::json node_json{};
      node_json["name"] = node->name;
      node_json["order"] = index++;

      node_json["state"] = [&node]() -> std::string {
        switch (node->state) {
          case execution::State::running:
            return "R";
          case execution::State::error:
            return "ER";
          case execution::State::finished:
            return "EX";
          case execution::State::needs_interaction:
            return "NI";
          default:
            return "TBE";
            break;
        }
      }();

      j[node->id] = std::move(node_json);
    }

    return j;
  }

  std::filesystem::path deprecated_tree_path(const std::filesystem::path &working_dir) noexcept {
    return state_dir(working_dir) / "deprecated_tree.json";
  }

  bool store_deprecated_tree_to_file(const Graph &graph, const std::filesystem::path &working_dir) noexcept {
    return utility::save_file(deprecated_tree_path(working_dir), build_tree(graph).dump(2));
  }

  std::filesystem::path deprecated_status_path(const std::filesystem::path &working_dir) noexcept {
    return state_dir(working_dir) / "deprecated_status.json";
  }

  bool store_deprecated_status_to_file(const Workflow &workflow, const std::filesystem::path &working_dir) noexcept {
    auto state = nlohmann::json(workflow.state).get<std::string>();
    state[0] = toupper(state[0]);

    nlohmann::json j{};
    j["startDateTime"] = workflow.started_at == 0 ? "" : utility::to_old_engine_time_format(std::chrono::system_clock::time_point(std::chrono::seconds(workflow.started_at)));
    j["endDateTime"] = workflow.finished_at == 0 ? "" : utility::to_old_engine_time_format(std::chrono::system_clock::time_point(std::chrono::seconds(workflow.finished_at)));
    j["fileName"] = workflow.file.string();
    j["iteration"] = 0;
    j["nodesProcessed"] = workflow.nodes_finished;
    j["nodesProcessedInLoops"] = 0;
    j["nodesTotal"] = workflow.nodes.size();
    j["pid"] = -1;
    // TODO: Where does this name come from?
    j["processEngine"] = "Process-Engine";
    j["state"] = state;
    j["variables"] = workflow.variables;

    return utility::save_file(deprecated_status_path(working_dir), j.dump(2));
  }
} // namespace process_engine::workflow