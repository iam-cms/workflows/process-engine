#pragma once

#include "execution/context.hpp"
#include "workflow/workflow.hpp"

#include <filesystem>
#include <optional>

namespace process_engine::workflow {
  std::optional<Workflow> from_file(const std::filesystem::path &file) noexcept;

  [[nodiscard]] std::filesystem::path log_dir(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] std::filesystem::path execution_log_path(const std::filesystem::path &working_dir) noexcept;

  [[nodiscard]] std::filesystem::path ipc_dir(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] std::filesystem::path ipc_socket_path(const std::filesystem::path &working_dir) noexcept;

  [[nodiscard]] std::filesystem::path tmp_dir(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] std::filesystem::path state_dir(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] std::filesystem::path interactions_path(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] std::filesystem::path nodes_path(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] std::filesystem::path status_path(const std::filesystem::path &working_dir) noexcept;

  [[nodiscard]] bool load_node_states_from_file(Nodes &nodes, const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] bool store_node_states_to_file(const Nodes &nodes, const std::filesystem::path &working_dir) noexcept;

  [[nodiscard]] std::optional<Interactions> load_interactions_from_file(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] bool store_interactions_to_file(const Interactions &interactions, const std::filesystem::path &working_dir) noexcept;

  [[nodiscard]] bool load_status_from_file(Workflow &workflow, const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] bool store_status_to_file(const Workflow &workflow, const std::filesystem::path &working_dir) noexcept;

  [[nodiscard]] std::optional<std::filesystem::path> find_workflow_file(const std::filesystem::path &working_dir) noexcept;
} // namespace process_engine::workflow