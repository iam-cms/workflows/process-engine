/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "execution/state.hpp"
#include "graph.hpp"
#include "node/node.hpp"

#include <nlohmann/json.hpp>

#include <filesystem>
#include <list>
#include <memory>
#include <mutex>
#include <regex>
#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

namespace process_engine::workflow {
  using Nodes = std::vector<std::unique_ptr<node::Node>>;
  using Graph = graph::AdjacencyList<node::Node *>;

  struct Workflow final {
    Nodes nodes{};
    Graph graph{};
    Variables variables{};
    Interactions interactions{};
    std::vector<std::filesystem::path> artifacts{};

    std::filesystem::path file{};
    execution::State state{};
    int started_at{};
    size_t finished_at{};
    size_t nodes_finished{};

    void started_now() noexcept;
    void finished_now() noexcept;
  };

  struct GraphError final {
    using Nodes = std::pair<const node::Node *, const node::Node *>;
    using Ports = std::pair<const node::Port *, const node::Port *>;

    enum struct ErrorKind {
      none,
      has_cycles,
      wrong_connection,
      incompatible_port,
    };

    ErrorKind error{};
    std::variant<std::monostate, Nodes, Ports> items{};
  };

  /// @brief Checks if the graph is valid.
  /// @param graph The graph to check.
  /// @return Returns a list of errors.
  [[nodiscard]] std::vector<GraphError> validate(const Graph &graph) noexcept;

} // namespace process_engine::workflow