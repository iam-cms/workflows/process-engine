#include "interaction.hpp"

namespace process_engine::workflow {
  Interaction *find_interaction(const std::string &node_id, Interactions &interactions) noexcept {
    auto find_iter = std::find_if(interactions.begin(), interactions.end(), [&node_id](const auto &interaction) {
      return interaction.node_id == node_id;
    });

    return find_iter == interactions.end() ? nullptr : &*find_iter;
  }

  Interaction *find_or_add_interaction(const std::string &node_id, Interactions &interactions) noexcept {
    Interaction *interaction = find_interaction(node_id, interactions);

    if (interaction == nullptr) {
      interaction = &interactions.emplace_back();

      interaction->id = std::to_string(interactions.size() - 1);
      interaction->node_id = node_id;
    }

    return interaction;
  }
} // namespace process_engine::workflow