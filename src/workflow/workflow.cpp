#include "workflow.hpp"

#include "node/types.hpp"
#include "utility.hpp"

#include <nlohmann/json.hpp>

namespace process_engine::workflow {
  std::vector<GraphError> validate(const Graph &graph) noexcept {
    const auto topological_order = process_engine::graph::sort_topological(graph);
    std::vector<GraphError> errors{};

    if (!topological_order) {
      errors.emplace_back(GraphError::ErrorKind::has_cycles);
      return errors;
    }

    for (const auto &[src_node, ports] : graph) {
      for (const auto &[src_port_index, edge] : ports.outgoing) {
        const auto dst_node = edge.node;

        // Not connected.
        if (dst_node == nullptr) {
          continue;
        }

        // Check if ports exists.
        if (src_node->outputs.size() <= src_port_index || dst_node->inputs.size() <= edge.port_index) {
          errors.emplace_back(GraphError::ErrorKind::wrong_connection, GraphError::Nodes{src_node, dst_node});
          continue;
        }

        const auto &src_output_port = src_node->outputs.at(src_port_index);
        const auto &dst_input_port = dst_node->inputs.at(edge.port_index);

        if (!node::AvailableTypes::are_compatible(src_output_port.type, dst_input_port.type)) {
          errors.emplace_back(GraphError::ErrorKind::incompatible_port, GraphError::Ports{&src_output_port, &dst_input_port});
        }
      }
    }

    return errors;
  }

  void Workflow::started_now() noexcept {
    started_at = utility::to_unix_timestamp_seconds(std::chrono::system_clock::now());
    state = execution::State::running;
  }

  void Workflow::finished_now() noexcept {
    state = execution::State::finished;
    finished_at = utility::to_unix_timestamp_seconds(std::chrono::system_clock::now());
    nodes_finished = nodes.size();
  }
} // namespace process_engine::workflow