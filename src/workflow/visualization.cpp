#include "visualization.hpp"

#include "format/graphviz.hpp"

namespace process_engine::workflow {
  std::string to_graphviz(const Graph &graph, const std::vector<std::pair<std::string, std::string>> &icons) noexcept {
    std::stringstream result{};

    result << graphviz::begin_digraph("G", graphviz::GRAPH_DIR_LEFT_TO_RIGHT);

    for (const auto &graph_entry : graph) {
      const auto node = graph_entry.first;

      graphviz::NodeOptions options{};
      options.label = node->name;
      options.shape = graphviz::SHAPE_RECT;

      const auto icon_iter = std::find_if(icons.begin(), icons.end(), [&node](const auto &pair) {
        return pair.first == node->name;
      });

      if (icon_iter != icons.end()) {
        options.image = icon_iter->second;
      }

      result << graphviz::node(node->id, options);
    }

    for (const auto &graph_entry : graph) {
      const auto node = graph_entry.first;
      const auto &ports = graph_entry.second;

      for (const auto &output : ports.outgoing) {
        graphviz::ConnectionOptions options{};
        options.from_port = "e";
        options.to_port = "w";

        result << graphviz::connection(node->id, output.second.node->id, options);
      }
    }

    result << graphviz::end();

    return result.str();
  }
} // namespace process_engine::workflow