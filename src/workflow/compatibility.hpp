#pragma once

#include "execution/context.hpp"
#include "workflow/workflow.hpp"

#include <nlohmann/json.hpp>

#include <filesystem>

namespace process_engine::workflow {
  void make_interaction_compatible(nlohmann::json &interaction_json, int order) noexcept;
  [[nodiscard]] nlohmann::json build_tree(const Graph &graph) noexcept;

  [[nodiscard]] std::filesystem::path deprecated_tree_path(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] bool store_deprecated_tree_to_file(const Graph &graph, const std::filesystem::path &working_dir) noexcept;

  [[nodiscard]] std::filesystem::path deprecated_status_path(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] bool store_deprecated_status_to_file(const Workflow &workflow, const std::filesystem::path &working_dir) noexcept;

} // namespace process_engine::workflow