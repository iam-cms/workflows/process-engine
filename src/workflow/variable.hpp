#pragma once

#include <string>
#include <unordered_map>

namespace process_engine::workflow {
  using Variables = std::unordered_map<std::string, std::string>;
}