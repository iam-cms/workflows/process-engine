#include "utility.hpp"

#include <fstream>

namespace process_engine::utility {
  std::optional<std::string> read_file_as_string(const std::filesystem::path &path) noexcept {
    if (!std::filesystem::exists(path)) {
      return std::nullopt;
    }

    std::string content{};
    content.resize(std::filesystem::file_size(path));

    std::fstream file(path, std::ios::in | std::ios::binary);
    if (!file.is_open()) {
      return std::nullopt;
    }

    file.read(content.data(), content.size());
    file.close();

    return content;
  }

  std::optional<nlohmann::json> load_json(const std::filesystem::path &path) noexcept {
    const auto content = read_file_as_string(path);
    if (!content) {
      return std::nullopt;
    }

    if (!nlohmann::json::accept(*content)) {
      return std::nullopt;
    }

    return nlohmann::json::parse(*content);
  }

  bool save_file(const std::filesystem::path &path, std::string_view content) noexcept {
    std::fstream stream(path, std::ios::out);
    if (!stream.is_open()) {
      return false;
    }

    stream << content;
    stream.close();

    return true;
  }

  int to_unix_timestamp_seconds(const std::chrono::system_clock::time_point &time_point) noexcept {
    return static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(time_point.time_since_epoch()).count());
  }

  std::string to_old_engine_time_format(const std::chrono::system_clock::time_point &time_point) noexcept {
    const auto time = std::chrono::system_clock::to_time_t(time_point);
    std::tm *tm = std::localtime(&time);

    std::stringstream result{};
    result << std::put_time(tm, "%H:%M:%S %d.%m.%Y");

    return result.str();
  }

  std::vector<std::string> split(const std::string &value, std::string_view delimiter) noexcept {
    // Source: https://stackoverflow.com/a/48403210
    auto views = value |
                 std::ranges::views::split(delimiter) |
                 std::ranges::views::transform([](auto &&word) {
                   const auto size = std::ranges::distance(word);
                   return size ? std::string_view(&*word.begin(), size) : std::string_view();
                 });

    return std::vector<std::string>(views.begin(), views.end());
  }
} // namespace process_engine::utility