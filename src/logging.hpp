#pragma once

#include <spdlog/spdlog.h>

#include <filesystem>
#include <string_view>

namespace process_engine::logging {
  static constexpr std::string_view FORMAT{"{};{}"};

#define LOG_EXECUTION_INFO(logger, id, msg)               \
  logger->info(process_engine::logging::FORMAT, id, msg); \
  logger->flush()
#define LOG_EXECUTION_WARNING(logger, id, msg)            \
  logger->warn(process_engine::logging::FORMAT, id, msg); \
  logger->flush()
#define LOG_EXECUTION_ERROR(logger, id, msg)               \
  logger->error(process_engine::logging::FORMAT, id, msg); \
  logger->flush()

#define LOG_ENGINE_EXECUTION_INFO(logger, msg) LOG_EXECUTION_INFO(logger, "Process Engine", msg)
#define LOG_ENGINE_EXECUTION_WARNING(logger, msg) LOG_EXECUTION_WARNING(logger, "Process Engine", msg)
#define LOG_ENGINE_EXECUTION_ERROR(logger, msg) LOG_EXECUTION_ERROR(logger, "Process Engine", msg)

  void setup_execution_logging(const std::filesystem::path &execution_log_path, bool use_colors) noexcept;
  [[nodiscard]] std::shared_ptr<spdlog::logger> execution_logger() noexcept;
} // namespace process_engine::logging