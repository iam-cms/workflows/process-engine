#include "logging.hpp"

#include <spdlog/pattern_formatter.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/stdout_sinks.h>

namespace {
  class NodeIDFlag : public spdlog::custom_flag_formatter {
  public:
    void format(const spdlog::details::log_msg &msg, const std::tm &, spdlog::memory_buf_t &dest) override {
      const auto payload = std::string_view(msg.payload.begin(), msg.payload.end());
      const auto id = payload.substr(0, payload.find(";"));

      dest.append(id.data(), id.data() + id.size());
    }

    std::unique_ptr<custom_flag_formatter> clone() const override {
      return spdlog::details::make_unique<NodeIDFlag>();
    }
  };

  class NodeMsgFlag : public spdlog::custom_flag_formatter {
  public:
    void format(const spdlog::details::log_msg &msg, const std::tm &, spdlog::memory_buf_t &dest) override {
      const auto payload = std::string_view(msg.payload.begin(), msg.payload.end());
      const auto text = payload.substr(payload.find(";") + 1);

      dest.append(text.data(), text.data() + text.size());
    }

    std::unique_ptr<custom_flag_formatter> clone() const override {
      return spdlog::details::make_unique<NodeMsgFlag>();
    }
  };
} // namespace


namespace process_engine::logging {
  void setup_execution_logging(const std::filesystem::path &execution_log_path, bool use_colors) noexcept {
    auto execution_logger = spdlog::basic_logger_mt("execution", execution_log_path.string());

    if (use_colors) {
      // execution_logger->sinks().push_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
    } else {
      // execution_logger->sinks().push_back(std::make_shared<spdlog::sinks::stdout_sink_mt>());
    }

    auto formatter = std::make_unique<spdlog::pattern_formatter>();
    formatter->add_flag<NodeIDFlag>('*');
    formatter->add_flag<NodeMsgFlag>(';');

    formatter->set_pattern("%*; [%Y-%m-%d %H:%M:%S][%^%l%$] %;");

    execution_logger->set_formatter(std::move(formatter));

#ifndef NDEBUG
    execution_logger->set_level(spdlog::level::debug);
#endif
  }

  std::shared_ptr<spdlog::logger> execution_logger() noexcept {
    return spdlog::get("execution");
  }
} // namespace process_engine::logging