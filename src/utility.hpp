#pragma once

#include <nlohmann/json.hpp>

#include <chrono>
#include <filesystem>
#include <iostream>
#include <optional>
#include <sstream>
#include <stop_token>
#include <string>
#include <string_view>

namespace process_engine {
  template<size_t Size>
  struct CompileTimeString final {
    constexpr CompileTimeString(const char (&string)[Size]) {
      for (size_t i = 0; i < Size; ++i) {
        value[i] = string[i];
      }
    }

    constexpr operator std::string_view() const {
      return std::string_view{value.data(), Size - 1};
    }

    std::array<char, Size> value{};
  };
} // namespace process_engine

namespace process_engine::utility {
  [[nodiscard]] std::optional<std::string> read_file_as_string(const std::filesystem::path &path) noexcept;
  [[nodiscard]] std::optional<nlohmann::json> load_json(const std::filesystem::path &path) noexcept;
  [[nodiscard]] bool save_file(const std::filesystem::path &path, std::string_view content) noexcept;
  [[nodiscard]] std::vector<std::string> split(const std::string &value, std::string_view delimiter) noexcept;

  [[nodiscard]] int to_unix_timestamp_seconds(const std::chrono::system_clock::time_point &time_point) noexcept;
  [[nodiscard]] std::string to_old_engine_time_format(const std::chrono::system_clock::time_point &time_point) noexcept;

  template<typename T>
  [[nodiscard]] std::string join(const std::vector<T> &values, const std::string &delimiter) noexcept {
    std::ostringstream os{};

    for (auto iter = values.cbegin(); iter != values.cend(); ++iter) {
      os << *iter << delimiter;
    }

    auto result = os.str();
    result.erase(result.size() - delimiter.size());

    return result;
  }
} // namespace process_engine::utility


NLOHMANN_JSON_NAMESPACE_BEGIN

// Allows serializing and deserializing contents behind a std::unique_ptr.
// See also: https://github.com/nlohmann/json/issues/975
template<typename T>
struct adl_serializer<std::unique_ptr<T>> {
  template<typename BasicJsonType>
  static void to_json(BasicJsonType &json_value, const std::unique_ptr<T> &ptr) {
    if (ptr.get()) {
      json_value = *ptr;
    } else {
      json_value = nullptr;
    }
  }

  template<typename BasicJsonType>
  static void from_json(const BasicJsonType &json_value, std::unique_ptr<T> &ptr) {
    T inner_val = json_value.template get<T>();
    ptr = std::make_unique<T>(std::move(inner_val));
  }
};

NLOHMANN_JSON_NAMESPACE_END