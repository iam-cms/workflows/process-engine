#include "misc.hpp"

#include <fstream>
#include <regex>
#include <string>

namespace process_engine::node {
  std::unique_ptr<Node> create_format_string(std::string id, size_t input_count, std::string format) noexcept {
    auto format_node = FormatStringNode::make_unique(std::move(id));

    for (size_t i = 0; i < input_count; ++i) {
      auto port = StringPort<"">::create();
      port.name = "%" + std::to_string(i);
      format_node->inputs.emplace_back(std::move(port));
    }

    format_node->execute = [input_count, format = std::move(format)](Node *node, [[maybe_unused]] execution::Context &ctx) mutable {
      for (size_t i = 0; i < input_count; ++i) {
        const std::regex regex("%" + std::to_string(i));
        const auto &value = node->inputs.at(i + 1).value;

        format = std::regex_replace(format, regex, value);
      }

      node->outputs.at(1).value = format;

      return execution::State::finished;
    };

    return format_node;
  }
} // namespace process_engine::workflow