#include "source.hpp"

namespace {

  using namespace process_engine::node;

  template<typename T>
  std::unique_ptr<Node> create_source_node(std::string id, std::string value) noexcept {
    auto node = T::make_unique(std::move(id));
    node->outputs.at(0).value = std::move(value);
    return node;
  }
} // namespace

namespace process_engine::node {
  std::unique_ptr<Node> create_string_source(std::string id, std::string value) noexcept {
    return create_source_node<StringSource>(std::move(id), std::move(value));
  }

  std::unique_ptr<Node> create_int_source(std::string id, std::string value) noexcept {
    return create_source_node<IntSource>(std::move(id), std::move(value));
  }

  std::unique_ptr<Node> create_float_source(std::string id, std::string value) noexcept {
    return create_source_node<FloatSource>(std::move(id), std::move(value));
  }

  std::unique_ptr<Node> create_bool_source(std::string id, std::string value) noexcept {
    return create_source_node<BoolSource>(std::move(id), std::move(value));
  }
} // namespace process_engine::workflow