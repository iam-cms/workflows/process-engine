#pragma once

#include "node.hpp"

namespace process_engine::node {
  using UserOutputText = DefineNode<"UserOutputText">::with_inputs<DependenciesPort, StringPort<"Text File">>::with_outputs<DependentsPort>;
  using UserOutputWebView = DefineNode<"UserOutputWebView">::with_inputs<DependenciesPort, StringPort<"Description">, StringPort<"Url">>::with_outputs<DependentsPort>;

  [[nodiscard]] std::unique_ptr<Node> create_user_output_text(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_user_output_web_view(std::string id) noexcept;
}