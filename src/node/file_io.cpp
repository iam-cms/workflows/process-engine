#include "file_io.hpp"

#include <fstream>
#include <regex>

namespace {
  using namespace process_engine;
  using namespace process_engine::node;

  [[nodiscard]] execution::State handle_file_schema(Node *node, execution::Context &ctx, const std::string &path) noexcept {
    const auto file_size = std::filesystem::file_size(path);

    auto &output = node->outputs[*FileInputNode::outputs::index_of("stdout")].value;
    output.resize(file_size);

    // TODO: Large files can cause problems.
    // TODO: Binary files can cause problems when state is stored in JSON.
    // TODO: Caching?
    std::ifstream stream(path, std::ios::in | std::ios::binary);
    if (!stream.is_open()) {
      LOG_ENGINE_EXECUTION_ERROR(ctx.logger, fmt::format("Could not open file '{}'", path));
      return execution::State::error;
    }

    stream.read(output.data(), output.size());
    stream.close();

    return execution::State::finished;
  }
} // namespace

namespace process_engine::node {
  std::unique_ptr<Node> create_file_input(std::string id) noexcept {
    auto input_node = FileInputNode::make_unique(std::move(id));

    input_node->execute = [](Node *node, execution::Context &ctx) {
      auto file_path = node->inputs.at(*FileInputNode::inputs::index_of("File path")).to_string();

      std::error_code error{};
      if (!std::filesystem::exists(file_path, error)) {
        return execution::State::error;
      }

      static std::regex schema_regex(R"(^(\w+):\/\/(.*))");

      std::string schema{"file"};

      std::smatch match{};
      if (std::regex_match(file_path, match, schema_regex)) {
        schema = match[1].str();
        file_path = match[2].str();
      }

      if (schema == "file") {
        return handle_file_schema(node, ctx, file_path);
      }

      LOG_ENGINE_EXECUTION_ERROR(ctx.logger, fmt::format("Unkown schema '{}'", schema));
      return execution::State::error;
    };

    return input_node;
  }

  std::unique_ptr<Node> create_file_output(std::string id, bool is_artifact) noexcept {
    auto output_node = FileOutputNode::make_unique(std::move(id));

    output_node->execute = [is_artifact](Node *node, execution::Context &ctx) {
      auto file_path = node->inputs[*FileOutputNode::inputs::index_of("File path")].to_string();
      const auto append = node->inputs[*FileOutputNode::inputs::index_of("Append")].to_bool();
      const auto &std_in = node->inputs[*FileOutputNode::inputs::index_of("stdin")].to_string();

      auto flags = std::ios::out | std::ios::binary;

      if (append) {
        flags |= std::ios::app;
      }

      // TODO: Create whole directory structure before failing to open the file?
      std::ofstream stream(file_path, flags);
      if (!stream.is_open()) {
        LOG_EXECUTION_ERROR(ctx.logger, node->id, "Could not open file");
        return execution::State::error;
      }

      stream.write(std_in.data(), std_in.size());
      stream.close();

      if (is_artifact) {
        ctx.artifacts.push_back(file_path);
      }

      return execution::State::finished;
    };

    return output_node;
  }
} // namespace process_engine::node