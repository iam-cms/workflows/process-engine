#pragma once

#include "node.hpp"

namespace process_engine::node {
  using FormatStringNode = DefineNode<"FormatString">::with_inputs<DependenciesPort>::with_outputs<DependentsPort, StringPort<"Formatted String">>;

  [[nodiscard]] std::unique_ptr<Node> create_format_string(std::string id, size_t input_count, std::string format) noexcept;
} // namespace process_engine::workflow