#include "user_output.hpp"

#include "logging.hpp"

#include <fstream>

namespace process_engine::node {
  std::unique_ptr<Node> create_user_output_text(std::string id) noexcept {
    auto user_output_node = UserOutputText::make_unique(std::move(id));

    user_output_node->execute = [](Node *node, execution::Context &ctx) {
      const auto find_interaction = workflow::find_interaction(node->id, ctx.interactions);
      if (find_interaction) {
        return execution::State::finished;
      }

      auto interaction = find_or_add_interaction(node->id, ctx.interactions);
      interaction->direction = workflow::Interaction::Direction::output;
      interaction->type = type_to_string<std::string>();

      const auto &file_path = node->inputs[1].to_string();
      if (file_path.empty()) {
        return execution::State::finished;
      }

      std::ifstream stream(file_path, std::ios::in);
      if (!stream.is_open()) {
        LOG_EXECUTION_ERROR(ctx.logger, node->id, fmt::format("Could not open file '{}'", file_path));
        return execution::State::error;
      }

      // TODO: Also use URL's?
      std::error_code error_code{};
      const auto file_size = std::filesystem::file_size(file_path, error_code);
      if (error_code) {
        LOG_EXECUTION_ERROR(ctx.logger, node->id, error_code.message());
        return execution::State::error;
      }

      interaction->description.resize(file_size);
      stream.read(interaction->description.data(), interaction->description.size());

      return execution::State::needs_interaction;
    };

    return user_output_node;
  }

  std::unique_ptr<Node> create_user_output_web_view(std::string id) noexcept {
    auto user_output_node = UserOutputWebView::make_unique(std::move(id));

    user_output_node->execute = [](Node *node, execution::Context &ctx) {
      const auto find_interaction = workflow::find_interaction(node->id, ctx.interactions);
      if (find_interaction) {
        return execution::State::finished;
      }

      const auto &desc = node->inputs[1].to_string();
      const auto &url = node->inputs[2].to_string();

      if (url.empty()) {
        return execution::State::finished;
      }

      auto interaction = find_or_add_interaction(node->id, ctx.interactions);
      interaction->direction = workflow::Interaction::Direction::output;
      interaction->type = "webView";
      interaction->url = url;
      interaction->description = desc;

      return execution::State::needs_interaction;
    };

    return user_output_node;
  }
} // namespace process_engine::node