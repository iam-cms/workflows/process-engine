/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "types.hpp"
#include "utility.hpp"

#include <nlohmann/json.hpp>

#include <string>
#include <string_view>

// TODO: Part of process_engine::workflow?
namespace process_engine::node {

  /// @brief Input or output of a node.
  struct Port {
    using ValueType = std::string;

    std::string name{};
    std::string type{};
    ValueType value{};

    [[nodiscard]] bool is_empty() const noexcept {
      return value.empty();
    }

    [[nodiscard]] bool to_bool() const noexcept {
      return (value == "1" || value == "true") ? true : false;
    }

    [[nodiscard]] int to_int() const noexcept {
      return std::stoi(value);
    }

    [[nodiscard]] int to_int_or_default(int i) const noexcept {
      return value.empty() ? i : std::stoi(value);
    }

    [[nodiscard]] float to_float() const noexcept {
      return std::stof(value);
    }

    [[nodiscard]] const std::string &to_string() const noexcept {
      return value;
    }

    // TODO: Needed?
    nlohmann::json hints{};

    // TODO: Should not be part of the interface.
    NLOHMANN_DEFINE_TYPE_INTRUSIVE(Port, name, type, value, hints)
  };

  /// @brief Creates a port type.
  /// @tparam Type Type of the value.
  /// @tparam Name The name of the port.
  template<typename Type, CompileTimeString Name>
  struct PortDesc {
    static constexpr std::string_view name = Name;

    /// @brief Creates an instance of the defined port.
    /// @return The created port.
    static Port create() {
      return Port{
        .name = std::string(Name),
        .type = std::string(type_to_string<Type>()),
      };
    }
  };

  /// @brief Describes a list of ports e.g. inputs or outputs of a node.
  /// @tparam ...T The ports.
  template<typename... T>
  struct PortList {

    /// @brief Returns a port by a given index.
    /// @tparam Index The index of the port.
    template<std::size_t Index>
    using at = typename std::tuple_element<Index, std::tuple<T...>>::type;

    /// @brief Creates a list of ports defined by the template.
    /// @return The created list of ports.
    [[nodiscard]] static std::vector<Port> create() {
      return std::vector<Port>{(T::create())...};
    };

    /// @brief Checks if a port with name exists.
    /// @param name The name of the port.
    /// @return True if a port with the given name is found, false otherwise.
    [[nodiscard]] static constexpr bool contains(const std::string &name) noexcept {
      return ((T::name == name ? true : false) || ...);
    }

    /// @brief Tries to find the index of port by name.
    /// @param name The name of the port.
    /// @return The index or std::nullopt if the port could not be found.
    [[nodiscard]] static constexpr std::optional<size_t> index_of(std::string_view name) noexcept {
      std::optional<size_t> result{};
      size_t index = 0;

      ((T::name == name ? (result = index, true) : (++index, false)) || ...);

      return result;
    }
  };

  template<CompileTimeString Name>
  using StringPort = PortDesc<std::string, Name>;

  template<CompileTimeString Name>
  using IntPort = PortDesc<int, Name>;

  template<CompileTimeString Name>
  using BoolPort = PortDesc<bool, Name>;

  template<CompileTimeString Name>
  using FloatPort = PortDesc<float, Name>;

  using EnvPort = PortDesc<Env, "env">;

  template<CompileTimeString Name>
  using DependencyPort = PortDesc<Dependency, Name>;

  using DependenciesPort = DependencyPort<"Dependencies">;
  using DependentsPort = DependencyPort<"Dependents">;

  using StdinPort = PortDesc<std::string, "stdin">;
  using StdoutPort = PortDesc<std::string, "stdout">;
} // namespace process_engine