/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "user_input.hpp"

#include "logging.hpp"

namespace {
  using namespace process_engine;
  using namespace process_engine::node;
  using namespace process_engine::workflow;

  Interaction *common_interaction(Node *node, Interaction *interaction, int default_port = 2) {
    interaction->type = node->outputs.at(1).type;
    interaction->description = node->inputs.at(1).value.empty() ? "" : node->inputs.at(1).value;
    interaction->default_value = node->inputs.at(default_port).value;

    return interaction;
  }

  [[nodiscard]] bool check_interaction(Node *node, Interaction *interaction, execution::Context &ctx) noexcept {
    if (!interaction->value.is_null()) {
      return true;
    }

    std::stringstream log_msg{};
    log_msg << "Awaiting user input of type " << interaction->type;
    LOG_EXECUTION_INFO(ctx.logger, node->id, log_msg.str());

    return false;
  }

  [[nodiscard]] std::string to_port_value(const nlohmann::json &interation_value) noexcept {
    return interation_value.is_string() ? interation_value.get<std::string>() : interation_value.dump();
  }

  execution::State handle_common_interaction(Node *node, Interaction *interaction, execution::Context &ctx, int value_port = 1) {
    if (check_interaction(node, interaction, ctx)) {
      node->outputs.at(value_port).value = to_port_value(interaction->value);
      return execution::State::finished;
    }

    return execution::State::needs_interaction;
  }

  template<typename T>
  std::unique_ptr<Node> create_default_user_input_node(std::string id) noexcept {
    auto interaction_node = T::make_unique(std::move(id));

    interaction_node->execute = [](Node *node, execution::Context &ctx) {
      return handle_common_interaction(node, common_interaction(node, find_or_add_interaction(node->id, ctx.interactions)), ctx);
    };

    return interaction_node;
  }
} // namespace

namespace process_engine::node {
  std::unique_ptr<Node> create_user_input_text(std::string id) noexcept {
    auto user_input_node = UserInputText::make_unique(std::move(id));

    user_input_node->execute = [](Node *node, execution::Context &ctx) {
      auto interaction = find_or_add_interaction(node->id, ctx.interactions);
      common_interaction(node, interaction);

      interaction->multiline = node->inputs.at(3).to_bool();
      return handle_common_interaction(node, interaction, ctx);
    };

    return user_input_node;
  }

  std::unique_ptr<Node> create_user_input_integer(std::string id) noexcept {
    return create_default_user_input_node<UserInputInteger>(std::move(id));
  }

  std::unique_ptr<Node> create_user_input_bool(std::string id) noexcept {
    return create_default_user_input_node<UserInputBool>(std::move(id));
  }

  std::unique_ptr<Node> create_user_input_float(std::string id) noexcept {
    return create_default_user_input_node<UserInputFloat>(std::move(id));
  }

  std::unique_ptr<Node> create_user_input_select(std::string id) noexcept {
    auto select_node = UserInputSelect::make_unique(std::move(id));

    select_node->execute = [](Node *node, execution::Context &ctx) {
      // TODO: Validate value agains the options?
      auto interaction = find_or_add_interaction(node->id, ctx.interactions);

      common_interaction(node, interaction, 3);
      interaction->type = "select";

      const auto &delimiter = node->inputs.at(4).value;

      interaction->options = utility::split(node->inputs.at(2).value, delimiter.empty() ? "," : delimiter);
      return handle_common_interaction(node, interaction, ctx);
    };

    return select_node;
  }

  std::unique_ptr<Node> create_user_input_periodic_table(std::string id) noexcept {
    auto periodic_table_node = UserInputPeriodicTable::make_unique(std::move(id));

    periodic_table_node->execute = [](Node *node, execution::Context &ctx) {
      auto interaction = find_or_add_interaction(node->id, ctx.interactions);

      common_interaction(node, interaction);
      interaction->type = "periodicTable";

      return handle_common_interaction(node, interaction, ctx);
    };

    return periodic_table_node;
  }

  std::unique_ptr<Node> create_user_input_choose(std::string id, int input_count) noexcept {
    auto choose_node = UserInputChoose::make_unique(std::move(id));

    for (int i = 0; i < input_count; ++i) {
      auto input_port = StringPort<"">::create();
      input_port.name = "Option " + std::to_string(i + 1);

      choose_node->inputs.emplace_back(std::move(input_port));
    }

    choose_node->execute = [input_count](Node *node, execution::Context &ctx) {
      const auto default_port = node->inputs.at(2);

      auto interaction = find_or_add_interaction(node->id, ctx.interactions);
      interaction->type = "choose";
      interaction->description = node->inputs.at(1).value.empty() ? "" : node->inputs.at(1).value;

      if (!default_port.value.empty()) {
        const auto selected_option = std::max(std::min(default_port.to_int(), input_count), 1);

        // Jump over the other inputs.
        interaction->default_value = node->inputs.at(selected_option + 2).value;
      }

      std::vector<std::string> values{};

      for (int i = 0; i < input_count; ++i) {
        values.push_back(node->inputs.at(i + 3).value);
      }

      // Try to populate the value.
      const auto state = handle_common_interaction(node, interaction, ctx, 2);

      if (state == execution::State::finished) {
        const auto find_iter = std::find_if(values.begin(), values.end(), [&node](const auto &value) {
          return value == node->outputs.at(2).value;
        });

        // A invalid value is selected.
        if (find_iter == values.end()) {
          return execution::State::error;
        }

        node->outputs.at(1).value = std::to_string((find_iter - values.begin()) + 1);
      }

      interaction->options = std::move(values);

      return state;
    };

    return choose_node;
  }

  std::unique_ptr<Node> create_user_input_file(std::string id) noexcept {
    auto file_node = UserInputFile::make_unique(std::move(id));

    file_node->execute = [](Node *node, execution::Context &ctx) {
      auto interaction = find_or_add_interaction(node->id, ctx.interactions);

      common_interaction(node, interaction);
      interaction->type = "file";

      return handle_common_interaction(node, interaction, ctx);
    };

    return file_node;
  }

  std::unique_ptr<Node> create_user_input_form(std::string id) noexcept {
    auto form_node = UserInputForm::make_unique(std::move(id));

    form_node->execute = [](Node *node, execution::Context &ctx) {
      auto interaction = find_or_add_interaction(node->id, ctx.interactions);

      interaction->description = "";
      interaction->type = "form";
      interaction->url = node->inputs.at(1).value;

      return handle_common_interaction(node, interaction, ctx);
    };

    return form_node;
  }

  std::unique_ptr<Node> create_user_input_select_bounding_box(std::string id) noexcept {
    auto bounding_box_node = UserInputSelectBoundingBox::make_unique(std::move(id));

    bounding_box_node->execute = [](Node *node, execution::Context &ctx) {
      auto interaction = find_or_add_interaction(node->id, ctx.interactions);

      interaction->description = "";
      interaction->type = "cropImages";
      interaction->url = node->inputs.at(2).value;

      // TODO: Prefill and validate value?
      if (!check_interaction(node, interaction, ctx)) {
        return execution::State::needs_interaction;
      }

      if (!nlohmann::json::accept(interaction->value.get<std::string>())) {
        LOG_EXECUTION_INFO(ctx.logger, node->id, "Crop data is not valid JSON");
        return execution::State::error;
      }

      const auto crop_json = nlohmann::json::parse(interaction->value.get<std::string>());

      bool valid{true};

      // TODO: Better validation.
      for (const auto &object : crop_json) {
        valid &= object.contains("key") && object.contains("value");
      }

      if (!valid) {
        LOG_EXECUTION_INFO(ctx.logger, node->id, "Crop data is not valid");
        return execution::State::error;
      }

      for (size_t i = 1; i < 5; ++i) {
        node->outputs[i].value = to_port_value(crop_json[i]["value"]);
      }

      return execution::State::finished;
    };

    return bounding_box_node;
  }
} // namespace process_engine::node