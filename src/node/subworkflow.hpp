#pragma once

#include "node.hpp"

namespace process_engine::node {
  using WorkflowNode = DefineNode<"WorkflowNode">::with_inputs<DependenciesPort>::with_outputs<>;

  [[nodiscard]] std::unique_ptr<Node> create_workflow(std::string id, std::string workflow_file) noexcept;
} // namespace process_engine::workflow