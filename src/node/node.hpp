/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "port.hpp"

#include "execution/context.hpp"
#include "execution/control.hpp"
#include "execution/state.hpp"
#include "workflow/interaction.hpp"
#include "workflow/variable.hpp"

#include <nlohmann/json.hpp>

#include <functional>
#include <memory>
#include <string>
#include <string_view>

namespace process_engine::node {
  struct Node {
    using Executor = std::function<execution::State(Node *, execution::Context &)>;
    using BranchSelect = std::function<std::vector<int>(Node *, execution::Context &)>;

    enum class Type {
      tool,
      control,
      env
    };

    std::string id{};
    std::string name{};
    Type type{};
    std::vector<Port> inputs{};
    std::vector<Port> outputs{};

    execution::State state{};
    int token{};
    bool skip{};

    Executor execute{};
    BranchSelect select_branches{};

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(Node, id, name, inputs, outputs, state, token, skip)
  };

  NLOHMANN_JSON_SERIALIZE_ENUM(Node::Type, {{Node::Type::tool, "tool"}, {Node::Type::control, "control"}, {Node::Type::env, "env"}})

  /// @brief Describes a node type.
  /// @tparam Inputs The input ports.
  /// @tparam Outputs The output ports.
  /// @tparam Name The name of the node.
  /// @tparam Type The type of the node.
  template<CompileTimeString Name, typename Inputs, typename Outputs, Node::Type Type = Node::Type::tool>
  struct NodeDesc {
    using inputs = Inputs;
    using outputs = Outputs;
    static constexpr std::string_view name = Name;

    static Node create(std::string id) {
      Node node{};
      node.id = std::move(id);
      node.type = Type;
      node.name = std::string(Name);
      node.inputs = Inputs::create();
      node.outputs = Outputs::create();
      node.execute = [](Node *, execution::Context &) { return execution::State::finished; };
      node.select_branches = [](Node *, execution::Context &) { return std::vector<int>{}; };

      return node;
    };

    static std::unique_ptr<Node> make_unique(std::string id) {
      auto node = std::make_unique<Node>();
      node->id = std::move(id);
      node->type = Type;
      node->name = std::string(Name);
      node->inputs = Inputs::create();
      node->outputs = Outputs::create();
      node->execute = [](Node *, execution::Context &) { return execution::State::finished; };
      node->select_branches = [](Node *, execution::Context &) { return std::vector<int>{}; };

      return node;
    };
  };

  template<CompileTimeString Name, Node::Type Type = Node::Type::tool>
  struct DefineNode final {

    template<typename... Inputs>
    struct with_inputs final {

      template<typename... Outputs>
      using with_outputs = NodeDesc<Name, PortList<Inputs...>, PortList<Outputs...>, Type>;
    };
  };
} // namespace process_engine::node