/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "tool.hpp"

#include "logging.hpp"
#include "process.hpp"
#include "tools/tools.hpp"
#include "tools/xmlhelp.hpp"
#include "utility.hpp"

#include <tinyxml2.h>

#include <iostream>
#include <regex>
#include <sstream>

namespace {
  using namespace process_engine;
  using namespace process_engine::node;
  using namespace tinyxml2;

  std::vector<std::string> build_command_list(const std::string &program, const Node *node) {
    std::vector<std::string> arguments(utility::split(program, " "));
    std::vector<std::string> positionals{};

    for (const auto &input : node->inputs) {
      if (input.type == type_to_string<Dependency>() || input.name == EnvPort::name || input.name == StdinPort::name) {
        continue;
      }

      if (!input.hints.contains("inputBinding")) {
        continue;
      }

      if (input.hints["inputBinding"].contains("position")) {
        positionals.push_back(input.value);
      } else if (input.hints["inputBinding"].contains("prefix")) {
        const auto &prefix = input.hints["inputBinding"]["prefix"];
        if (input.type == type_to_string<bool>() && !input.value.empty() && input.to_bool()) {
          arguments.push_back(prefix);
        } else if (!input.value.empty()) {
          arguments.push_back(prefix);
          arguments.push_back(std::string(input.value));
        }
      }
    }

    arguments.insert(arguments.end(), positionals.begin(), positionals.end());
    return arguments;
  }

  execution::State execute_tool_node(const std::string &program, bool run_detached, Node *node, execution::Context &ctx) {
    auto cmd_list = build_command_list(program, node);

    const auto find_env_iter = std::find_if(node->inputs.cbegin(), node->inputs.cend(), [](const auto &port) {
      return port.name == EnvPort::name;
    });

    if (find_env_iter != node->inputs.cend() && !find_env_iter->value.empty()) {
      const auto &env_cmd = find_env_iter->to_string();
      auto env_cmd_list = utility::split(env_cmd, " ");

      std::ostringstream wrapped_cmd{};
      wrapped_cmd << "'" << utility::join(cmd_list, " ") << "'";
      env_cmd_list.push_back(wrapped_cmd.str());

      cmd_list = std::move(env_cmd_list);
    }

    if (run_detached) {
      if (process::run_detached(cmd_list)) {
        LOG_EXECUTION_INFO(ctx.logger, node->id, "Tool is started in detached mode");
        return execution::State::finished;
      }

      LOG_EXECUTION_ERROR(ctx.logger, node->id, "Tool could not be started in detached mode");

      return execution::State::error;
    }

    LOG_EXECUTION_INFO(ctx.logger, node->id, "Start execution");

    const auto result = process::run(cmd_list, std::chrono::milliseconds(0), ctx.control.cancel_source.get_token());

    const auto find_stdout_iter = std::find_if(node->outputs.begin(), node->outputs.end(), [](const auto &port) {
      return port.name == StdoutPort::name;
    });

    if (!result.out.empty()) {
      LOG_EXECUTION_INFO(ctx.logger, node->id, result.out);
    }

    if (find_stdout_iter != node->outputs.end()) {
      find_stdout_iter->value = std::move(result.out);
    }

    std::stringstream log_msg{};
    log_msg << "Execution finished [exit code: " << result.status << " ]";
    LOG_EXECUTION_INFO(ctx.logger, node->id, log_msg.str());

    // TODO: State based on exit code.
    return (result.error_code || result.status != 0) ? execution::State::error : execution::State::finished;
  }
} // namespace

namespace process_engine::node {
  std::unique_ptr<Node> create_tool(std::string id, std::vector<Port> inputs, std::vector<Port> outputs, std::string tool_name, std::string tool_path, bool run_detached) noexcept {
    auto tool_node = ToolNode::make_unique(std::move(id));
    tool_node->name = std::move(tool_name);

    tool_node->inputs = std::move(inputs);
    tool_node->outputs = std::move(outputs);

    tool_node->execute = [program = std::move(tool_path), run_detached](Node *node, execution::Context &ctx) {
      return execute_tool_node(program, run_detached, node, ctx);
    };

    return tool_node;
  }

  std::unique_ptr<Node> create_env(std::string id, std::string tool_path) noexcept {
    auto env_node = EnvNode::make_unique(std::move(id));

    env_node->execute = [program = std::move(tool_path)](Node *node, [[maybe_unused]] execution::Context &ctx) {
      // TODO: Check if correct.
      auto cmd_list = build_command_list(program, node);
      cmd_list.emplace_back("--env-exec");

      node->outputs.at(0).value = utility::join(cmd_list, " ");
      return execution::State::finished;
    };

    return env_node;
  }
} // namespace process_engine::node