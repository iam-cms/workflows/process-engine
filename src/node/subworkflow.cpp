#include "subworkflow.hpp"

#include "execution/execution.hpp"
#include "node/control.hpp"
#include "workflow/io.hpp"
#include "workflow/workflow.hpp"

using namespace process_engine::workflow;

namespace process_engine::node {
  std::unique_ptr<Node> create_workflow(std::string id, std::string workflow_file) noexcept {
    auto subworkflow_node = WorkflowNode::make_unique(std::move(id));

    auto subworkflow = workflow::from_file(workflow_file);
    if (!subworkflow) {
      return nullptr;
    }

    std::vector<Node *> found_variable_nodes{};

    // Find all variables that have no input to their value.
    for (const auto &graph_entry : subworkflow->graph) {
      if (graph_entry.first->name == Variable::name && graph_entry.second.incoming.count(Variable::inputs::index_of("value").value()) == 0) {
        found_variable_nodes.emplace_back(graph_entry.first);
      }
    }

    // Sort by ID to create some sort of port order.
    std::sort(found_variable_nodes.begin(), found_variable_nodes.end(), [](const Node *lhs, const Node *rhs) {
      return lhs->id < rhs->id;
    });

    for (auto variable_node : found_variable_nodes) {
      // Collect the inputs of the variable to get the name.
      auto result = execution::collect_inputs(*subworkflow->graph.find(variable_node), subworkflow->variables);
      if (!result) {
        // TODO: Handle error
      }

      auto variable_name = variable_node->inputs.at(Variable::inputs::index_of("name").value()).to_string();
      subworkflow_node->inputs.push_back(Port{variable_name, std::string(type_to_string<std::string>()), {}, {}});
    }

    // Captures in std::function must be copyable before C++23.
    // So a capture struct is used.
    struct CaptureCtx final {
      Workflow workflow{};
    };

    auto capture_ctx = std::make_shared<CaptureCtx>();
    capture_ctx->workflow = std::move(subworkflow.value());

    subworkflow_node->execute = [capture_ctx, workflow_file]([[maybe_unused]] Node *node, [[maybe_unused]] execution::Context &ctx) {
      [[maybe_unused]] Workflow &workflow = capture_ctx->workflow;

      // TODO: Implement.
      // Workflow workflow{};
      // if (!workflow.load(ctx.working_dir / "subworkflows" / node->id, workflow_file, std::move(capture_ctx->nodes), std::move(capture_ctx->graph), std::move(capture_ctx->variables))) {
      //   return execution::State::error;
      // }

      // TODO: Correct context.
      // execution::Context subctx{
      //   .control = ctx.control,
      //   .interactions = {},
      //   .variables = {},
      //   .working_dir = "",
      //   .variables = {},
      // };

      // subctx.working_dir = subworkflow->working_dir();

      // Inputs of a workflow node must be assigned to the according variable inside the subworkflow.
      // First input is the dependencies port and must be skipped.
      // for (size_t i = 1; i < node->inputs.size(); ++i) {
      //   const auto &input = node->inputs.at(i);

      //   // TODO: What todo when the value is empty?
      //   // Create an interaction?
      //   subctx.variables.insert({input.name, input.value.empty() ? "" : input.value});
      // }

      // TODO: What todo about interactions.
      // return run(workflow, subctx) ? execution::State::finished : execution::State::error;
      return execution::State::error;
    };

    return subworkflow_node;
  }

} // namespace process_engine::node