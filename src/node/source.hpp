/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "node.hpp"

namespace process_engine::node {

  using StringSource = DefineNode<"String">::with_inputs<>::with_outputs<StringPort<"value">>;
  using IntSource = DefineNode<"Integer">::with_inputs<>::with_outputs<IntPort<"value">>;
  using FloatSource = DefineNode<"Float">::with_inputs<>::with_outputs<IntPort<"value">>;
  using BoolSource = DefineNode<"Boolean">::with_inputs<>::with_outputs<BoolPort<"value">>;

  [[nodiscard]] std::unique_ptr<Node> create_string_source(std::string id, std::string value) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_int_source(std::string id, std::string value) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_float_source(std::string id, std::string value) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_bool_source(std::string id, std::string value) noexcept;

  [[nodiscard]] constexpr bool is_source_node(std::string_view name) noexcept {
    return name == StringSource::name || name == IntSource::name || name == FloatSource::name || name == BoolSource::name;
  }
} // namespace process_engine::node