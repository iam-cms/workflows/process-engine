/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "control.hpp"

#include "utility.hpp"

namespace {
  using namespace process_engine;
  using namespace process_engine::node;

  int loop_index(Node *node) noexcept {
    return node->outputs[2].to_int_or_default(0);
  }

  int end_index(Node *node) noexcept {
    return node->inputs[3].to_int_or_default(0);
  }

  bool has_loop_finished(Node *node) noexcept {
    // TODO: Exclusive or inclusive.
    return loop_index(node) >= end_index(node) + 1;
  }

  std::vector<int> select_if_branch(Node *node, [[maybe_unused]] execution::Context &ctx) {
    std::vector<int> branches{};
    branches.emplace_back(0);
    branches.emplace_back(node->inputs[1].to_bool() ? 1 : 2);

    return branches;
  }

  execution::State execute_loop(Node *node, [[maybe_unused]] execution::Context &ctx) {
    auto &index_port = node->outputs[2];
    auto &step_port = node->inputs[4];

    // TODO: Implement "Index variable name".

    index_port.value = index_port.is_empty() ? node->inputs[2].value : std::to_string(loop_index(node) + step_port.to_int_or_default(1));
    return has_loop_finished(node) ? execution::State::finished : execution::State::repeat;
  }

  std::vector<int> select_loop_branch(Node *node, [[maybe_unused]] execution::Context &ctx) {
    std::vector<int> result{};

    // TODO: Not the best idea to use the hints.
    // TODO: When node statistics are implemented, maybe use them.
    // Add the "after-loop" dependency only at the beginning to the selected branches.
    // Otherwise when the loop is repeated this path would also be executed again.
    if (!node->outputs[0].hints.contains("executed")) {
      node->outputs[0].hints["executed"] = true;
      result.emplace_back(0);
    }

    if (!has_loop_finished(node)) {
      result.emplace_back(1);
    }

    return result;
  }

  std::vector<int> select_branch(Node *node, [[maybe_unused]] execution::Context &ctx) {
    std::vector<int> result{};
    result.emplace_back(0);

    const auto &selected_port = node->inputs.at(1);
    if (!selected_port.value.empty()) {
      const size_t selected = selected_port.to_int();

      // TODO: What todo when index out of range?
      if (selected > 0 && selected < node->outputs.size()) {
        // Add one because port 0 is the "Dependents" port.
        // TODO: Is selected 0 or 1 based, because the port names start with 1.
        result.emplace_back(selected);
      }
    }


    // TODO: Select branch.

    return result;
  }

  execution::State execute_variable(Node *node, execution::Context &ctx) {
    const auto &name_port = node->inputs.at(1);
    const auto &value_port = node->inputs.at(2);

    // TODO: Check if already defined?
    if (!name_port.value.empty()) {
      auto iter = ctx.variables.insert({name_port.to_string(), {}});

      if (!value_port.value.empty()) {
        iter.first->second = value_port.to_string();
      }
    }

    return execution::State::finished;
  }

  execution::State execute_variable_list(Node *node, execution::Context &ctx) {
    const auto &names_port = node->inputs.at(1);
    const auto &values_port = node->inputs.at(2);
    const auto &delimiter_port = node->inputs.at(3);

    const std::string delimiter = delimiter_port.value.empty() ? ";" : delimiter_port.value;

    const auto names = utility::split(names_port.value, delimiter);
    const auto values = utility::split(values_port.value, delimiter);

    // TODO: Or error?
    if (names.size() != values.size()) {
      return execution::State::finished;
    }

    for (size_t i = 0; i < names.size(); ++i) {
      ctx.variables.insert({names.at(i), values.at(i)});
    }

    return execution::State::finished;
  }
} // namespace

namespace process_engine::node {
  std::unique_ptr<Node> create_if_branch(std::string id) noexcept {
    auto node = IfBranch::make_unique(std::move(id));
    node->select_branches = select_if_branch;
    return node;
  }

  std::unique_ptr<Node> create_loop(std::string id) noexcept {
    auto node = Loop::make_unique(std::move(id));
    node->execute = execute_loop;
    node->select_branches = select_loop_branch;

    return node;
  }

  std::unique_ptr<Node> create_branch_select(std::string id, size_t branch_count) noexcept {
    auto node = BranchSelectNode::make_unique(std::move(id));

    for (size_t i = 0; i < branch_count; ++i) {
      auto dep_port = DependencyPort<"">::create();
      dep_port.name = "Branch " + std::to_string(i + 1);

      node->outputs.emplace_back(std::move(dep_port));
    }

    node->select_branches = select_branch;
    return node;
  }

  std::unique_ptr<Node> create_variable(std::string id) noexcept {
    auto node = Variable::make_unique(std::move(id));
    node->execute = execute_variable;

    return node;
  }

  std::unique_ptr<Node> create_variable_list(std::string id) noexcept {
    auto node = VariableList::make_unique(std::move(id));
    node->execute = execute_variable_list;

    return node;
  }
} // namespace process_engine::node