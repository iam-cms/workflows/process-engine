/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "node.hpp"

namespace process_engine::node {
  using IfBranch = DefineNode<"IfBranch", Node::Type::control>::with_inputs<DependenciesPort, BoolPort<"Condition">>::with_outputs<DependentsPort, DependencyPort<"True">, DependencyPort<"False">>;
  using Loop = DefineNode<"Loop", Node::Type::control>::with_inputs<DependenciesPort, DependencyPort<"Condition">, IntPort<"Start Index">, IntPort<"End Index">, IntPort<"Step">, StringPort<"Index Variable Name">>::with_outputs<DependentsPort, DependencyPort<"Loop">, IntPort<"Index">>;
  using BranchSelectNode = DefineNode<"BranchSelect", Node::Type::control>::with_inputs<DependenciesPort, IntPort<"Selected">>::with_outputs<DependentsPort>;

  using Variable = DefineNode<"Variable">::with_inputs<DependenciesPort, StringPort<"name">, StringPort<"value">>::with_outputs<DependentsPort>;
  using VariableList = DefineNode<"VariableList">::with_inputs<DependenciesPort, StringPort<"names">, StringPort<"values">, StringPort<"delimiter [default ;]">>::with_outputs<DependentsPort>;

  [[nodiscard]] std::unique_ptr<Node> create_if_branch(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_loop(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_branch_select(std::string id, size_t branch_count) noexcept;

  [[nodiscard]] std::unique_ptr<Node> create_variable(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_variable_list(std::string id) noexcept;
} // namespace process_engine::workflow
