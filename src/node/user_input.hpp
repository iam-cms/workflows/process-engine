/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "node.hpp"

namespace process_engine::node {

  using UserInputText = DefineNode<"UserInputText">::with_inputs<DependenciesPort, StringPort<"Prompt">, StringPort<"Default">, BoolPort<"Multiline">>::with_outputs<DependentsPort, StringPort<"Value">>;
  using UserInputInteger = DefineNode<"UserInputInteger">::with_inputs<DependenciesPort, StringPort<"Prompt">, IntPort<"Default">>::with_outputs<DependentsPort, IntPort<"Value">>;
  using UserInputBool = DefineNode<"UserInputBool">::with_inputs<DependenciesPort, StringPort<"Prompt">, BoolPort<"Default">>::with_outputs<DependentsPort, BoolPort<"Value">>;
  using UserInputFloat = DefineNode<"UserInputFloat">::with_inputs<DependenciesPort, StringPort<"Prompt">, FloatPort<"Default">>::with_outputs<DependentsPort, FloatPort<"Value">>;
  using UserInputSelect = DefineNode<"UserInputSelect">::with_inputs<DependenciesPort, StringPort<"Prompt">, StringPort<"Values">, StringPort<"Default">, StringPort<"Delimiter [default: ,]">>::with_outputs<DependentsPort, StringPort<"Value">>;
  using UserInputPeriodicTable = DefineNode<"UserInputPeriodicTable">::with_inputs<DependenciesPort, StringPort<"Prompt">, StringPort<"Default">>::with_outputs<DependentsPort, StringPort<"Value">>;
  using UserInputChoose = DefineNode<"UserInputChoose">::with_inputs<DependenciesPort, StringPort<"Prompt">, IntPort<"Default">>::with_outputs<DependentsPort, IntPort<"Selected">, StringPort<"Value">>;
  using UserInputFile = DefineNode<"UserInputFile">::with_inputs<DependenciesPort, StringPort<"Prompt">, StringPort<"Default">>::with_outputs<DependentsPort, StringPort<"Value">>;
  using UserInputForm = DefineNode<"UserInputForm">::with_inputs<DependenciesPort, StringPort<"JSON File">>::with_outputs<DependentsPort, StringPort<"JSON String">>;
  using UserInputSelectBoundingBox = DefineNode<"UserInputSelectBoundingBox">::with_inputs<DependenciesPort, StringPort<"Prompt">, StringPort<"Image Path">>::with_outputs<DependentsPort, IntPort<"X">, IntPort<"Y">, IntPort<"Width">, IntPort<"Height">>;

  [[nodiscard]] std::unique_ptr<Node> create_user_input_text(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_user_input_integer(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_user_input_bool(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_user_input_float(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_user_input_select(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_user_input_periodic_table(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_user_input_choose(std::string id, int input_count) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_user_input_file(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_user_input_form(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_user_input_select_bounding_box(std::string id) noexcept;
} // namespace process_engine::node