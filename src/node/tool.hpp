/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "node.hpp"

namespace process_engine::node {
  using ToolNode = DefineNode<"ToolNode">::with_inputs<>::with_outputs<>;
  using EnvNode = DefineNode<"EnvNode">::with_inputs<>::with_outputs<>;

  [[nodiscard]] std::unique_ptr<Node> create_tool(std::string id, std::vector<Port> inputs, std::vector<Port> outputs, std::string tool_name, std::string tool_path, bool run_detached) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_env(std::string id, std::string tool_path) noexcept;
} // namespace process_engine::workflow