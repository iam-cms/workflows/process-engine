/* Copyright 2024 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
#pragma once

#include <nlohmann/json.hpp>

#include <string_view>

namespace process_engine::node {
  struct Env final {};
  struct Dependency final {};

  template<typename T>
  constexpr std::string_view type_to_string() {
    static_assert(sizeof(T) == 0, "No type_to_string implemented for this type");
    return "unkown";
  }

  template<>
  constexpr std::string_view type_to_string<std::string>() {
    return "string";
  }

  template<>
  constexpr std::string_view type_to_string<int>() {
    return "int";
  }

  template<>
  constexpr std::string_view type_to_string<float>() {
    return "float";
  }

  template<>
  constexpr std::string_view type_to_string<bool>() {
    return "bool";
  }

  template<>
  constexpr std::string_view type_to_string<Env>() {
    return "env";
  }

  template<>
  constexpr std::string_view type_to_string<Dependency>() {
    return "dependency";
  }

  template<typename T, typename... Comp>
  struct Type {
    using type = T;
    static constexpr std::string_view name = type_to_string<T>();

    template<typename U>
    [[nodiscard]] static constexpr bool compatible_with() noexcept {
      return compatible_with(type_to_string<U>());
    }

    [[nodiscard]] static constexpr bool compatible_with(std::string_view other) noexcept {
      return type_to_string<T>() == other || ((type_to_string<Comp>() == other ? true : false) || ...);
    }

    [[nodiscard]] static nlohmann::json to_json() noexcept {
      return std::vector<std::string_view>{type_to_string<T>(), (type_to_string<Comp>())...};
    }
  };

  template<typename T>
  struct DefineType {
    template<typename... Comp>
    using compatible_with = Type<T, Comp...>;
  };

  template<typename... T>
  struct TypeList {
    static constexpr bool contains(std::string_view type) {
      return ((type_to_string<typename T::type>() == type ? true : false) || ...);
    }

    static constexpr bool are_compatible(std::string_view src_type, std::string_view dst_type) {
      return ((type_to_string<typename T::type>() == src_type ? T::compatible_with(dst_type) : false) || ...);
    }

    [[nodiscard]] static nlohmann::json to_json() noexcept {
      nlohmann::json result{};
      ((result[type_to_string<typename T::type>()] = T::to_json()), ...);

      return result;
    }
  };

  using StringType = DefineType<std::string>::compatible_with<int, float, bool>;
  using IntType = DefineType<int>::compatible_with<std::string, float, bool>;
  using BoolType = DefineType<bool>::compatible_with<std::string, int>;
  using FloatType = DefineType<float>::compatible_with<std::string>;
  using DependencyType = DefineType<Dependency>::compatible_with<>;
  using EnvType = DefineType<Env>::compatible_with<>;

  using AvailableTypes = TypeList<StringType, IntType, BoolType, FloatType, DependencyType, EnvType>;
} // namespace process_engine