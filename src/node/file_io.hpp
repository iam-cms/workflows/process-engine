#pragma once

#include "node.hpp"

namespace process_engine::node {
  using FileInputNode = DefineNode<"FileInput">::with_inputs<DependenciesPort, StringPort<"File path">>::with_outputs<DependentsPort, StdoutPort>;
  using FileOutputNode = DefineNode<"FileOutput">::with_inputs<DependenciesPort, StringPort<"File path">, BoolPort<"Append">, StdinPort>::with_outputs<DependentsPort>;

  [[nodiscard]] std::unique_ptr<Node> create_file_input(std::string id) noexcept;
  [[nodiscard]] std::unique_ptr<Node> create_file_output(std::string id, bool is_artifact) noexcept;
} // namespace process_engine::workflow