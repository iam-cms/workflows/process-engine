#pragma once

#include <chrono>
#include <stop_token>
#include <string>
#include <system_error>
#include <vector>

namespace process_engine::process {
  struct Result {
    int status{};
    std::error_code error_code{};

    // TODO: Rename to a usefull name, but not stdout and stderr because these are already defined names under windows.
    std::string out{};
    std::string err{};
  };

  [[nodiscard]] Result run(const std::vector<std::string> &args, std::chrono::milliseconds timeout, std::stop_token terminate_token) noexcept;
  [[nodiscard]] bool run_detached(const std::vector<std::string> &args) noexcept;
} // namespace process_engine::process