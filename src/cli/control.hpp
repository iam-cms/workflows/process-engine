#pragma once

#include <CLI/App.hpp>

#include <filesystem>
#include <string>

namespace process_engine::cli {
  struct RunOptions final {
    std::string workflow_file{};
    bool no_color{};
    bool exit{};
  };

  [[nodiscard]] int run_workflow(const std::filesystem::path &working_dir, RunOptions &options) noexcept;
  [[nodiscard]] int cancel_workflow(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] int continue_workflow(const std::filesystem::path &working_dir, std::string_view process_engine_program) noexcept;
  [[nodiscard]] int stop_workflow(const std::filesystem::path &working_dir) noexcept;

} // namespace process_engine::cli