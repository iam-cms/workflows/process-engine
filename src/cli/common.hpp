#pragma once

#include <CLI/App.hpp>

#include <string>

namespace process_engine::cli {
  struct PathValidator : public CLI::Validator {
    PathValidator() {
      name_ = "Path Validator";
      func_ = [](std::string &input) -> std::string {
        std::error_code error_code{};
        const auto canonical_path = std::filesystem::canonical(input, error_code);

        if (error_code) {
          std::stringstream error{};
          error << "'" << input << "': " << error_code.message();
          return error.str();
        }

        input = canonical_path.string();
        return "";
      };
    }
  };

  const static PathValidator path_validator;

  struct CommonOptions final {
    std::string working_dir{};
  };

  void add_common_options(CLI::App *app, CommonOptions &options) noexcept;
} // namespace process_engine::cli