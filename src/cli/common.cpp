#include "common.hpp"

#include <sstream>

namespace process_engine::cli {
  void add_common_options(CLI::App *app, CommonOptions &options) noexcept {
    app->add_option("-p,--path", options.working_dir, "Working directory")->required(true)->transform(path_validator);
  }
} // namespace process_engine::cli