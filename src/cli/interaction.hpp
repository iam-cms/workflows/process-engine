#pragma once

#include <CLI/CLI.hpp>

namespace process_engine::cli {
  [[nodiscard]] int list_interactions(const std::filesystem::path &working_dir) noexcept;

  struct InputOptions final {
    std::string interaction_id{};
    std::string value{};
  };

  [[nodiscard]] int add_input(const std::filesystem::path &working_dir, const InputOptions &options) noexcept;
} // namespace process_engine::cli