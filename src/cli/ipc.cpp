#include "ipc.hpp"

#include "ipc/http.hpp"
#include "workflow/io.hpp"

using namespace process_engine::ipc;

namespace process_engine::cli {
  IPCResult send_command(const std::filesystem::path &working_dir, std::string_view path) noexcept {
    const auto socket_path = workflow::ipc_socket_path(working_dir);

    Client client{};
    if (!client.connect(socket_path)) {
      return IPCResult::connection_error;
    }

    // TODO: Send correct request.
    // send_get_request(client, path);
    send_post_request(client, path);

    return IPCResult::success;
  }

  std::optional<Client> connect_to_instance(const std::filesystem::path &working_dir) noexcept {
    const auto socket_path = workflow::ipc_socket_path(working_dir);

    std::error_code error{};
    if (!std::filesystem::exists(socket_path, error)) {
      return std::nullopt;
    }

    Client client{};
    if (!client.connect(socket_path)) {
      return std::nullopt;
    }

    return client;
  }
} // namespace process_engine::cli