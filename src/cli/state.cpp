#include "state.hpp"

#include "cli/ipc.hpp"
#include "utility.hpp"
#include "workflow/compatibility.hpp"
#include "workflow/io.hpp"

namespace process_engine::cli {
  int workflow_status(const std::filesystem::path &working_dir) noexcept {
    const auto status = utility::read_file_as_string(workflow::deprecated_status_path(working_dir));
    if (!status) {
      return 1;
    }

    std::cout << status.value() << '\n';

    return 0;
  }

  int show_log(const std::filesystem::path &working_dir) noexcept {
    const auto content = utility::read_file_as_string(workflow::execution_log_path(working_dir));
    if (!content) {
      return 1;
    }

    std::cout << content.value() << '\n';

    return 0;
  }

  int print_log_path(const std::filesystem::path &working_dir) noexcept {
    const auto path = workflow::execution_log_path(working_dir);

    std::error_code error_code{};
    const auto absolute_path = std::filesystem::canonical(path, error_code);
    if (!error_code) {
      std::cout << absolute_path.string() << '\n';
    }

    return error_code.value();
  }

  int print_tree_path(const std::filesystem::path &working_dir) noexcept {
    std::cout << workflow::deprecated_tree_path(working_dir).string() << '\n';
    return 0;
  }

  int list_artifacts(const std::filesystem::path &working_dir) noexcept {
    workflow::Workflow workflow{};
    if (!workflow::load_status_from_file(workflow, working_dir)) {
      return 1;
    }

    nlohmann::json json{};
    json["shortcuts"] = workflow.artifacts;

    std::cout << json.dump(2) << '\n';
    return 0;
  }
} // namespace process_engine::cli