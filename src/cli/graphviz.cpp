#include "graphviz.hpp"

#include "workflow/io.hpp"
#include "workflow/visualization.hpp"
#include "workflow/workflow.hpp"

namespace process_engine::cli {
  CLI::App *add_graphviz_cmd(CLI::App *parent, GraphvizOptions &options) noexcept {
    auto cmd = parent->add_subcommand("graphviz");
    cmd->add_option("-e,--exclude", options.excluded_nodes, "Excluded nodes");
    cmd->add_option("-i,--icon", options.icons, "Icons");
    cmd->add_option("workflow", options.workflow_file, "Path to the workflow file")->required(true);

    return cmd;
  }

  int create_graphviz(GraphvizOptions &options) noexcept {
    auto workflow = workflow::from_file(options.workflow_file);
    if (!workflow) {
      return 1;
    }

    std::vector<node::Node *> nodes_to_remove{};

    for (const auto &graph_entry : workflow->graph) {
      if (options.excluded_nodes.contains(graph_entry.first->name)) {
        nodes_to_remove.push_back(graph_entry.first);
      }
    }

    for (const auto node : nodes_to_remove) {
      graph::exclude_node(workflow->graph, node);
    }

    std::cout << workflow::to_graphviz(workflow->graph, options.icons) << '\n';

    return 0;
  }
} // namespace process_engine::cli