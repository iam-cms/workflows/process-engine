#pragma once

#include <CLI/CLI.hpp>

#include <set>

namespace process_engine::cli {
  struct GraphvizOptions final {
    std::string workflow_file{};
    std::set<std::string> excluded_nodes{};
    std::vector<std::pair<std::string, std::string>> icons{};
  };

  [[nodiscard]] CLI::App *add_graphviz_cmd(CLI::App *parent, GraphvizOptions &options) noexcept;
  [[nodiscard]] int create_graphviz(GraphvizOptions &options) noexcept;
} // namespace process_engine::cli