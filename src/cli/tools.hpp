#pragma once

#include <CLI/CLI.hpp>

#include <vector>

namespace process_engine::cli {
  [[nodiscard]] CLI::App *add_tools_cmd(CLI::App *parent) noexcept;
  [[nodiscard]] CLI::App *add_list_tools_cmd(CLI::App *parent) noexcept;

  [[nodiscard]] int list_tools() noexcept;
} // namespace process_engine::cli