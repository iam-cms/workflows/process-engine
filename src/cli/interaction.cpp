#include "interaction.hpp"

#include "cli/ipc.hpp"
#include "ipc/http.hpp"
#include "workflow/compatibility.hpp"
#include "workflow/io.hpp"

#include <iostream>

namespace process_engine::cli {
  int list_interactions(const std::filesystem::path &working_dir) noexcept {
    nlohmann::json json{
      {
        "interactions",
        workflow::load_interactions_from_file(working_dir).value(),
      },
    };

    int order{};
    for (auto &interaction : json["interactions"]) {
      workflow::make_interaction_compatible(interaction, order++);
    }

    std::cout << json << '\n';
    return 0;
  }

  int add_input(const std::filesystem::path &working_dir, const InputOptions &options) noexcept {
    auto client = connect_to_instance(working_dir);

    // Send to the running instance.
    if (client) {
      std::stringstream path{};
      path << "/interactions/" << options.interaction_id;

      nlohmann::json json{};
      json["value"] = std::move(options.value);

      ipc::send_patch_request(client.value(), path.str(), json.dump());
      return 0;
    }

    auto interactions = workflow::load_interactions_from_file(working_dir);

    auto find_iter = std::find_if(interactions->begin(), interactions->end(), [&options](const auto &interaction) {
      return interaction.id == options.interaction_id;
    });

    if (find_iter == interactions->end()) {
      return 1;
    }

    find_iter->value = std::move(options.value);

    return workflow::store_interactions_to_file(interactions.value(), working_dir) ? 0 : 1;
  }
} // namespace process_engine::cli