#include "tools.hpp"

#include "node/control.hpp"
#include "node/source.hpp"
#include "node/user_input.hpp"
#include "tools/search.hpp"
#include "tools/tools.hpp"
#include "tools/xmlhelp.hpp"
#include "utility.hpp"

#include <tinyxml2.h>

#include <filesystem>
#include <fstream>
#include <iostream>

using namespace tinyxml2;

namespace process_engine::cli {
  CLI::App *add_tools_cmd(CLI::App *parent) noexcept {
    return parent->add_subcommand("tools");
  }

  CLI::App *add_list_tools_cmd(CLI::App *parent) noexcept {
    return parent->add_subcommand("list");
  }

  int list_tools() noexcept {
    using namespace process_engine::node;

    std::vector<Node> nodes{
      StringSource::create(""),
      IntSource::create(""),
      FloatSource::create(""),
      BoolSource::create(""),

      IfBranch::create(""),
      Loop::create(""),
      Variable::create(""),
    };

    std::cout << nlohmann::json(nodes).dump(2) << '\n';

    // name, outputs, inputs
    return 0;
  }
} // namespace process_engine::cli