#include "control.hpp"

#include "cli/common.hpp"
#include "cli/ipc.hpp"
#include "execution/control.hpp"
#include "execution/execution.hpp"
#include "execution/setup.hpp"
#include "ipc/content.hpp"
#include "ipc/control.hpp"
#include "ipc/http.hpp"
#include "ipc/interaction.hpp"
#include "ipc/node.hpp"
#include "logging.hpp"
#include "process.hpp"
#include "workflow/io.hpp"
#include "workflow/workflow.hpp"

using namespace process_engine::execution;

namespace {
  using namespace process_engine;
  using namespace process_engine::cli;
  using namespace process_engine::execution;

  bool change_state(const std::filesystem::path &working_dir, std::string_view endpoint, State state) noexcept {
    const auto send_result = send_command(working_dir, endpoint);
    if (send_result == IPCResult::success) {
      return 0;
    }

    // No instance is running, check if the status file can be loaded.
    std::error_code error_code{};
    if (!std::filesystem::exists(workflow::status_path(working_dir), error_code)) {
      return 1;
    }

    workflow::Workflow workflow{};
    if (!workflow::load_status_from_file(workflow, working_dir)) {
      return 1;
    }

    switch (workflow.state) {
      case State::pending:
      case State::running:
      case State::needs_interaction:
      case State::repeat:
        workflow.state = state;
        break;

      default:
        break;
    }

    return workflow::store_status_to_file(workflow, working_dir) ? 0 : 1;
  }

  bool change_to_stop_state(const std::filesystem::path &working_dir) noexcept {
    return change_state(working_dir, "/stop", State::stopped);
  }

  bool change_to_cancel_state(const std::filesystem::path &working_dir) noexcept {
    return change_state(working_dir, "/cancel", State::cancelled);
  }

} // namespace

namespace process_engine::cli {
  int run_workflow(const std::filesystem::path &working_dir, RunOptions &options) noexcept {
    const auto logger = logging::execution_logger();

    // TODO: What todo if the working dir already exists and the run command is used?
    const auto workflow_file_path = setup_working_dir(working_dir, options.workflow_file);
    if (!workflow_file_path) {
      return 1;
    }

    auto workflow = workflow::from_file(workflow_file_path.value());
    if (!workflow) {
      LOG_ENGINE_EXECUTION_ERROR(logger, "Could not parse workflow");
      return 1;
    }

    if (!prepare_execution(workflow.value(), working_dir)) {
      LOG_ENGINE_EXECUTION_ERROR(logger, "Could not prepare workflow for execution");
      return 1;
    }

    execution::Control execution_control{};

    execution::Context execution_ctx{
      .control = execution_control,
      .interactions = workflow->interactions,
      .variables = workflow->variables,
      .artifacts = workflow->artifacts,
      .working_dir = working_dir,
      .exit_when_no_work = options.exit,
      .logger = logging::execution_logger(),
    };

    ipc::HttpRoutes http_routes{};

    http_routes.merge(ipc::create_workflow_control_routes(workflow.value(), execution_control));
    http_routes.merge(ipc::create_workflow_interaction_routes(workflow.value(), execution_ctx));
    http_routes.merge(ipc::create_workflow_node_routes(workflow.value()));

#ifndef NDEBUG
    // TODO: Via CLI argument.
    if (auto static_content_routes = ipc::create_static_content_routes("_html")) {
      http_routes.merge(std::move(static_content_routes.value()));
    }
#endif

    ipc::Server server{workflow::ipc_socket_path(working_dir)};

    std::jthread server_thread([&server, &http_routes, &execution_ctx, &logger](std::stop_token stop_token) {
      if (!ipc::serve_http_server(server, http_routes, stop_token)) {
        LOG_ENGINE_EXECUTION_ERROR(logger, "Could not start IPC-Server");
        execution_ctx.exit_when_no_work = true;
      }
    });

    if (!execution::execute(workflow.value(), execution_ctx)) {
      LOG_ENGINE_EXECUTION_ERROR(logger, "Could not execute workflow");
      return 1;
    }

    server.stop();

    return 0;
  }

  int cancel_workflow(const std::filesystem::path &working_dir) noexcept {
    return change_to_cancel_state(working_dir);
  }

  int continue_workflow(const std::filesystem::path &working_dir, std::string_view process_engine_program) noexcept {
    std::error_code error_code{};

    // First try to reach a running instance.
    const auto result = send_command(working_dir, "/continue");
    if (result == IPCResult::success) {
      return 0;
    }

    // No running instance was found, but if the working directory exists, an instance can be lauchned.
    if (std::filesystem::exists(working_dir, error_code)) {
      const auto workflow_file = workflow::find_workflow_file(working_dir);
      if (!workflow_file) {
        return 1;
      }

      // Run an instance without side-effects.
      // When no node can be processed the engine terminates.
      return process::run_detached({std::string(process_engine_program), "run", "-e", "-p", working_dir.string(), workflow_file.value().string()}) ? 0 : 1;
    }

    // No instance is running and no working directory exists.
    // There is no way the specified workflow can be continued.
    return 1;
  }

  int stop_workflow(const std::filesystem::path &working_dir) noexcept {
    return change_to_stop_state(working_dir) ? 0 : 1;
  }
} // namespace process_engine::cli