#pragma once

#include <filesystem>

namespace process_engine::cli {
  [[nodiscard]] int workflow_status(const std::filesystem::path& working_dir) noexcept;
  [[nodiscard]] int show_log(const std::filesystem::path& working_dir) noexcept;
  [[nodiscard]] int print_log_path(const std::filesystem::path& working_dir) noexcept;
  [[nodiscard]] int print_tree_path(const std::filesystem::path& working_dir) noexcept;
  [[nodiscard]] int list_artifacts(const std::filesystem::path& working_dir) noexcept;
} // namespace process_engine::cli