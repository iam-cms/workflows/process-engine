#pragma once

#include "ipc/client.hpp"

#include <filesystem>
#include <optional>
#include <string_view>

namespace process_engine::cli {
  // TODO: This should be moved to ipc.
  enum struct IPCResult {
    success,
    socket_not_found,
    connection_error
  };

  IPCResult send_command(const std::filesystem::path &working_dir, std::string_view path) noexcept;
  std::optional<ipc::Client> connect_to_instance(const std::filesystem::path &working_dir) noexcept;
} // namespace process_engine::cli