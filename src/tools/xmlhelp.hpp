#pragma once

#include <filesystem>
#include <optional>
#include <string>
#include <vector>

namespace process_engine::tools {
  [[nodiscard]] std::vector<std::string> list_commands(const std::filesystem::path &program) noexcept;
  [[nodiscard]] std::optional<std::string> load_xmlhelp(const std::filesystem::path &program) noexcept;
  [[nodiscard]] std::optional<int> is_xmlhelp_positional(const std::string &name) noexcept;
} // namespace process_engine::tools
