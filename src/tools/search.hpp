#pragma once

#include <filesystem>
#include <vector>

namespace process_engine::tools {
  [[nodiscard]] bool is_executable(const std::filesystem::path &path) noexcept;
  std::vector<std::filesystem::path> search(const std::filesystem::path &path) noexcept;
} // namespace process_engine
