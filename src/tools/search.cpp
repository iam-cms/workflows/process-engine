#include "search.hpp"

#include "xmlhelp.hpp"

#include <filesystem>
#include <iostream>

namespace process_engine::tools {
  bool is_executable(const std::filesystem::path &path) noexcept {
    std::error_code error{};

    const auto status = std::filesystem::status(path, error);
    if (error) {
      return false;
    }

    using namespace std::filesystem;
    const auto required_perms = perms::owner_exec | perms::group_exec | perms::others_exec;

    return (status.permissions() & required_perms) != std::filesystem::perms::none;
  }

  std::vector<std::filesystem::path> search(const std::filesystem::path &path) noexcept {
    std::vector<std::filesystem::path> tools{};

    for (const auto &dir_entry : std::filesystem::directory_iterator(path)) {
      if (!is_executable(dir_entry.path())) {
        continue;
      }

      std::cout << "Checking " << dir_entry.path().string() << '\n';

      const auto commands = list_commands(dir_entry.path());
      if (commands.empty()) {
        continue;
      }

      // TODO: Read XMLHELP?
      tools.push_back(dir_entry.path());
      /*tools.insert(tools.end(), )*/
    }

    return tools;
  }
} // namespace process_engine::tools
