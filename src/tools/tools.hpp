#pragma once

#include <filesystem>
#include <optional>
#include <string>
#include <tuple>
#include <unordered_map>

namespace process_engine::tools {
  using Tools = std::unordered_map<std::string, std::string>;

  [[nodiscard]] std::optional<Tools> load_desc_from_file(const std::filesystem::path &path) noexcept;
  [[nodiscard]] std::optional<std::pair<std::string, std::string>> load_tool(std::string program) noexcept;
  [[nodiscard]] bool save_tools_to_file(const std::filesystem::path &path, const Tools &tools) noexcept;
} // namespace process_engine::tools
