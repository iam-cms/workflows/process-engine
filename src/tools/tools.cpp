#include "tools.hpp"

#include "utility.hpp"
#include "xmlhelp.hpp"

#include <tinyxml2.h>

#include <fstream>
#include <regex>

using namespace tinyxml2;

namespace process_engine::tools {
  std::optional<Tools> load_desc_from_file(const std::filesystem::path &path) noexcept {
    std::fstream config_file(path, std::ios::in);
    if (!config_file.is_open()) {
      return std::nullopt;
    }

    Tools tools{};

    std::string line{};
    while (std::getline(config_file, line)) {
      const auto separator = line.find(';');

      if (separator != std::string::npos) {
        tools.insert({line.substr(0, separator), line.substr(separator + 1)});
      } else {
        tools.insert({std::move(line), ""});
      }
    }

    return tools;
  }

  std::optional<std::pair<std::string, std::string>> load_tool(std::string program) noexcept {
    const auto xmlhelp = load_xmlhelp(program);
    if (!xmlhelp) {
      return std::nullopt;
    }

    XMLDocument doc{};

    XMLError result = doc.Parse(xmlhelp->c_str(), xmlhelp->size());
    if (result != XML_SUCCESS) {
      return std::nullopt;
    }

    XMLPrinter printer(nullptr, true);
    doc.Print(&printer);

    // The XMLPrinter replaces XML newlines &#10; with real newlines, which deforms the output.
    // The \n are converted back to &#10;
    return std::make_pair(std::move(program), std::regex_replace(std::string(printer.CStr(), printer.CStrSize() - 1), std::regex("\n"), "&#10;"));
  }

  bool save_tools_to_file(const std::filesystem::path &path, const Tools &tools) noexcept {
    std::fstream config_file(path, std::ios::out);
    if (!config_file.is_open()) {
      return false;
    }

    for (const auto &tool : tools) {
      config_file << tool.first << ';' << tool.second << '\n';
    }

    return true;
  }
} // namespace process_engine::tools