#include "xmlhelp.hpp"

#include "utility.hpp"

#include <regex>

namespace process_engine::tools {
  std::vector<std::string> list_commands(const std::filesystem::path &program) noexcept {
    std::stop_source stop_source{};

    // TODO: Implement.
    return {};
    // const auto [status, error, stdout, stderr] = utility::run_process(std::vector<std::string>{program.string(), "--commands"}, stop_source.get_token());
    // if (status != 0 || error) {
    //   return {};
    // }

    // return utility::split(stdout, "\n");
  }

  std::optional<std::string> load_xmlhelp(const std::filesystem::path &program) noexcept {
    // TODO: Implement.
    return std::nullopt;

    // auto args = utility::split(program, " ");
    // args.emplace_back("--xmlhelp");

    // std::stop_source stop_source{};

    // const auto [status, error, stdout, stderr] = utility::run_process(args, stop_source.get_token());
    // if (error || status != 0) {
    //   return {};
    // }

    // return (error || status != 0) ? std::nullopt : std::optional<std::string>(std::move(stdout));
  }

  std::optional<int> is_xmlhelp_positional(const std::string &name) noexcept {
    const std::regex pattern(R"(^(arg(\d+))$)");

    std::smatch matches{};

    if (std::regex_search(name, matches, pattern)) {
      return std::stoi(matches[2]);
    }

    return std::nullopt;
  }
} // namespace process_engine::tools
