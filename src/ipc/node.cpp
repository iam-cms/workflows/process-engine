#include "interaction.hpp"

namespace process_engine::ipc {
  HttpRoutes create_workflow_node_routes(workflow::Workflow &workflow) noexcept {
    const auto get_nodes = [&workflow]([[maybe_unused]] const ipc::HttpRequest &request) {
      // TODO: Good idea to dump the complete state?
      return create_http_response(200, nlohmann::json(workflow.nodes).dump(2), "application/json");
    };

    return HttpRoutes{
      {
        "GET",
        HttpRoute{
          .regex = std::regex("/nodes"),
          .handler = get_nodes,
        },
      },
    };
  }
} // namespace process_engine::ipc