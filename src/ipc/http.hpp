#pragma once

#include "client.hpp"
#include "server.hpp"

#include <map>
#include <optional>
#include <regex>
#include <unordered_map>

namespace process_engine::ipc {
  using HttpRequestHeaders = std::unordered_map<std::string_view, std::string_view>;
  using HttpResponseHeaders = std::unordered_map<std::string_view, std::string>;

  struct HttpStartLine final {
    std::string_view method{};
    std::string_view path{};
  };

  struct HttpRequest final {
    HttpStartLine start_line{};
    HttpRequestHeaders headers{};
    std::string_view body{};
    std::vector<std::string> route_params{};
  };

  [[nodiscard]] std::optional<HttpStartLine> parse_http_start_line(std::string_view request) noexcept;
  [[nodiscard]] std::optional<HttpRequestHeaders> parse_http_request_headers(std::string_view request) noexcept;

  template<typename String>
  [[nodiscard]] std::string to_string(const std::unordered_map<std::string_view, String> &headers) noexcept {
    std::stringstream result{};

    for (const auto &kv : headers) {
      result << kv.first << ": " << kv.second << "\r\n";
    }

    return result.str();
  }

  [[nodiscard]] std::string create_http_response(int status_code, std::string content = {}, std::string content_type = "text/html") noexcept;
} // namespace process_engine::ipc

namespace process_engine::ipc {
  struct HttpRoute final {
    std::regex regex{};
    std::function<std::string(const HttpRequest &request)> handler{};
  };

  using HttpRoutes = std::multimap<std::string_view, HttpRoute>;

  bool serve_http_server(Server &server, const HttpRoutes &routes, std::stop_token stop_token) noexcept;

  // TODO: Use std::optional<HttpResponse> as return value.
  void send_get_request(Client &client, std::string_view path) noexcept;
  void send_post_request(Client &client, std::string_view path) noexcept;
  void send_patch_request(Client &client, std::string_view path, std::string_view json) noexcept;
} // namespace process_engine::ipc