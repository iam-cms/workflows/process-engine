#pragma once

#include "socket.hpp"

#include <functional>
#include <queue>
#include <sstream>
#include <thread>
#include <unordered_map>

namespace process_engine::ipc {

  class Server final {
  public:
    explicit Server(std::filesystem::path socket_path) noexcept;
    Server(const Server &) = delete;
    Server(Server &&) = default;
    ~Server();

    Server &operator=(const Server &) = delete;
    Server &operator=(Server &&) = default;

    [[nodiscard]] bool start(std::stop_token stop_token) noexcept;
     void stop() noexcept;

    void write_to_client(const Socket &client, std::string content) noexcept;

    template<typename F>
    void on_received(F &&f) {
      on_data_received_ = std::move(f);
    }

  private:
    void work(std::stop_token stop_token) noexcept;
    void process_output(const Socket &client) noexcept;

  private:
    std::filesystem::path socket_path_{};
    Socket socket_{};

    struct OutputStream final {
      std::string buffer{};
      size_t bytes_send{};
    };

    std::unordered_map<Socket::socket_type, std::queue<OutputStream>> outputs_{};

    std::function<void(const Socket &, std::span<const char>)> on_data_received_{};
  };
} // namespace process_engine::ipc