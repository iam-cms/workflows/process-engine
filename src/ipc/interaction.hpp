#pragma once

#include "ipc/http.hpp"
#include "workflow/workflow.hpp"

namespace process_engine::ipc {
  [[nodiscard]] HttpRoutes create_workflow_interaction_routes(workflow::Workflow &workflow, const execution::Context& ctx) noexcept;
}