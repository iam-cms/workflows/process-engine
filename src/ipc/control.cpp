#include "control.hpp"

#include "execution/execution.hpp"
#include "workflow/compatibility.hpp"
#include "workflow/io.hpp"

using namespace process_engine::workflow;
using namespace process_engine::execution;

namespace process_engine::ipc {
  HttpRoutes create_workflow_control_routes(Workflow &workflow, execution::Control &control) noexcept {
    return HttpRoutes{
      {
        "POST",
        HttpRoute{
          .regex = std::regex("/continue"),
          .handler = [&workflow, &control]([[maybe_unused]] const HttpRequest &request) {
            clear_needs_interaction_states(workflow.nodes);
            control.resume();

            return create_http_response(200, "{}", "application/json");
          },
        },
      },
      {
        "POST",
        HttpRoute{
          .regex = std::regex("/stop"),
          .handler = [&control]([[maybe_unused]] const HttpRequest &request) {
            control.stop();
            return ipc::create_http_response(200, "{}", "application/json");
          },
        },
      },
      {
        "POST",
        HttpRoute{
          .regex = std::regex("/cancel"),
          .handler = [&control]([[maybe_unused]] const HttpRequest &request) {
            control.cancel();
            return ipc::create_http_response(200, "{}", "application/json");
          },
        },
      },
    };
  }
} // namespace process_engine::ipc