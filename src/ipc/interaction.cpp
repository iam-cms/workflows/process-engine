#include "interaction.hpp"

#include "workflow/compatibility.hpp"
#include "workflow/io.hpp"

using namespace process_engine::workflow;
using namespace process_engine::execution;

namespace {
  using namespace process_engine::ipc;

  [[nodiscard]] std::string get_interactions([[maybe_unused]] const HttpRequest &request, const Workflow &workflow) {
    return create_http_response(200, nlohmann::json(workflow.interactions).dump(2), "application/json");
  }

  [[nodiscard]] std::string post_interactions(const HttpRequest &request, Workflow &workflow, const Context &ctx) {
    // TODO: Check if request is json
    const auto interaction_id = request.route_params[0];

    auto find_iter = std::find_if(workflow.interactions.begin(), workflow.interactions.end(), [&interaction_id](const auto &interaction) {
      return interaction.id == interaction_id;
    });

    if (find_iter == workflow.interactions.end()) {
      return create_http_response(404, "[]", "application/json");
    }

    const auto body_json = nlohmann::json::parse(request.body);

    find_iter->value = body_json["value"];

    if (!store_interactions_to_file(workflow.interactions, ctx.working_dir)) {
      return create_http_response(500, "{}", "application/json");
    }

    return create_http_response(200, nlohmann::json(*find_iter).dump(), "application/json");
  }
} // namespace

namespace process_engine::ipc {
  HttpRoutes create_workflow_interaction_routes(Workflow &workflow, const Context &ctx) noexcept {
    return HttpRoutes{
      {
        "GET",
        HttpRoute{
          .regex = std::regex("/interactions"),
          .handler = [&workflow](const HttpRequest &request) {
            return get_interactions(request, workflow);
          },
        },
      },
      {
        "PATCH",
        HttpRoute{
          .regex = std::regex("/interactions/(\\w+)"),
          .handler = [&workflow, &ctx](const HttpRequest &request) {
            return post_interactions(request, workflow, ctx);
          },
        },
      }};
  }
} // namespace process_engine::ipc