#pragma once

#include "socket.hpp"

#include <optional>

namespace process_engine::ipc {
  class Client final {
  public:
    Client() = default;
    Client(const Client &) = delete;
    Client(Client &&) = default;
    ~Client();

    Client &operator=(const Client &) = delete;
    Client &operator=(Client &&) = default;

    [[nodiscard]] bool connect(const std::filesystem::path &path) noexcept;
    void disconnect() noexcept;

    [[nodiscard]] std::optional<std::vector<unsigned char>> send(std::span<const unsigned char> buffer) noexcept;

  private:
    Socket socket_{};
  };
} // namespace process_engine::ipc