#pragma once

#include "ipc/http.hpp"

#include <filesystem>
#include <optional>

namespace process_engine::ipc {
  [[nodiscard]] std::optional<HttpRoutes> create_static_content_routes(const std::filesystem::path &base_dir) noexcept;
}