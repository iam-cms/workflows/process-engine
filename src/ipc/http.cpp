#include "http.hpp"

#include <algorithm>
#include <charconv>
#include <unordered_map>

namespace process_engine::ipc {
  std::optional<HttpStartLine> parse_http_start_line(std::string_view request) noexcept {
    const auto method_end = request.find(' ');
    if (method_end == std::string_view::npos) {
      return {};
    }

    const auto path_end = request.find(' ', method_end + 1);
    if (path_end == std::string_view::npos) {
      return {};
    }

    HttpStartLine start_line{};
    start_line.method = request.substr(0, method_end);
    start_line.path = request.substr(method_end + 1, path_end - method_end - 1);

    return start_line;
  }

  std::optional<HttpRequestHeaders> parse_http_request_headers(std::string_view request) noexcept {
    using KeyValue = std::pair<std::string_view, std::string_view>;

    const auto header_end = request.find("\r\n\r\n");
    if (header_end == std::string_view::npos && header_end == 0) {
      return {};
    }

    request = request.substr(0, header_end + 4);
    // Jump over start line.
    request = request.substr(request.find("\r\n") + 2);

    const auto get_next_line = [&request]() -> std::string_view {
      const auto line_end = request.find("\r\n");

      if (line_end == std::string_view::npos) {
        return {};
      }

      const auto line = request.substr(0, line_end);
      if (line.empty()) {
        return {};
      }

      request = request.substr(line_end + 2);

      return line;
    };

    // TODO: Should be utility function.
    const auto trim = [](std::string_view s) -> std::string_view {
      s = s.substr(s.find_first_not_of(' '));
      s = s.substr(0, s.find_last_not_of(' ') + 1);
      return s;
    };

    // TODO: Should be utility funciton.
    const auto split = [&trim](std::string_view s, std::string_view separator) -> KeyValue {
      const auto index = s.find(separator);
      if (index == std::string_view::npos) {
        return {};
      }

      const auto key = s.substr(0, index);
      const auto value = s.substr(index + separator.size());

      return {trim(key), trim(value)};
    };

    HttpRequestHeaders headers{};

    for (auto line = get_next_line(); !line.empty(); line = get_next_line()) {
      const auto kv = split(line, ":");
      if (kv.first.empty() || kv.second.empty()) {
        return {};
      }

      headers.insert(std::move(kv));
    }

    return headers;
  }

  std::string create_http_response(int status_code, std::string content, std::string content_type) noexcept {
    HttpResponseHeaders headers{};
    headers.insert({"Connection", "close"});
    headers.insert({"Content-Type", std::move(content_type)});
    headers.insert({"Content-Length", std::to_string(content.size())});

    std::stringstream result{};
    result << "HTTP/1.1 " << status_code << "\r\n";
    result << to_string(headers);
    result << "\r\n";
    result << std::move(content);

    return result.str();
  }
} // namespace process_engine::ipc

namespace process_engine::ipc {
  struct HttpRequestInput final {
    std::vector<char> buffer{};
    std::optional<HttpRequest> request{};

    [[nodiscard]] std::string_view buffer_to_string_view() const noexcept {
      return std::string_view(buffer.data(), buffer.size());
    }
  };

  bool serve_http_server(Server &server, const HttpRoutes &routes, std::stop_token stop_token) noexcept {
    std::unordered_map<Socket::socket_type, HttpRequestInput> client_inputs{};

    server.on_received([&server, &routes, &client_inputs](const ipc::Socket &client, std::span<const char> buffer) {
      // TODO: Handle this in the on_connect.
      // A new client has connected.
      if (!client_inputs.contains(client.handle())) {
        client_inputs.insert({client.handle(), {}});
      }

      // Store the received data for the client.
      auto &input = client_inputs.at(client.handle());
      input.buffer.insert(input.buffer.end(), buffer.begin(), buffer.end());

      // Check if the max content size is reached.
      if (input.buffer.size() > 1024 * 1024) {
        server.write_to_client(client, create_http_response(413));
        client_inputs.erase(client.handle());
        return;
      }

      const auto buffer_view = input.buffer_to_string_view();
      const auto end_of_header = buffer_view.find("\r\n\r\n");

      // Check whether a header line exists or whether the buffer contains the header line that can be parsed.
      if (!input.request && end_of_header != std::string_view::npos) {
        const auto start_line = parse_http_start_line(buffer_view);
        const auto headers = parse_http_request_headers(buffer_view);

        // The request is faulty.
        if (!start_line || !headers) {
          server.write_to_client(client, create_http_response(400));
          client_inputs.erase(client.handle());
          return;
        }

        input.request = HttpRequest{
          .start_line = std::move(start_line.value()),
          .headers = std::move(headers.value()),
        };
      }

      // Check if body is fully received.
      if (input.request && input.request->headers.contains("Content-Length")) {
        const auto content_length_header = input.request->headers.at("Content-Length");

        int content_length;
        auto result = std::from_chars(content_length_header.data(), content_length_header.data() + content_length_header.size(), content_length);

        // Garbage in the header.
        if (result.ec == std::errc::invalid_argument) {
          server.write_to_client(client, create_http_response(400));
          client_inputs.erase(client.handle());
          return;
        }

        // TODO: Size of the header must be subtracted from the buffer size.
        // There is some content missing.
        // if (content_length < input.buffer.size()) {
        //   return;
        // }
      }

      // Header is received.
      if (input.request) {
        input.request->body = buffer_view.substr(end_of_header + 4);
        const auto method = input.request->start_line.method;

        // There is no method registered for the route.
        if (!routes.contains(method)) {
          server.write_to_client(client, create_http_response(405));
        }

        std::match_results<std::string_view::iterator> match_results{};

        const auto &range = routes.equal_range(method);

        // TODO: Path can contain query parameter.
        // Try to find a route that matches the path.
        const auto route_iter = std::find_if(range.first, range.second, [&input, &match_results](const auto &r) {
          auto path = input.request->start_line.path;
          return std::regex_match(path.begin(), path.end(), match_results, r.second.regex);
        });

        if (route_iter != range.second) {
          for (size_t i = 1; i < match_results.size(); ++i) {
            input.request->route_params.push_back(match_results.str(i));
          }

          server.write_to_client(client, route_iter->second.handler(input.request.value()));
        } else {
          server.write_to_client(client, create_http_response(404));
        }

        client_inputs.erase(client.handle());
      }
    });

    if (!server.start(stop_token)) {
      return false;
    }

    // TODO: Handle connection.
    // TODO: Handle disconnection.
    /*
      server.on_disconnect([](){});
      server.on_connect([](){});
    */

    return true;
  }

  void send_get_request(Client &client, std::string_view path) noexcept {
    std::stringstream r{};
    r << "GET " << path << " HTTP/1.1\r\n";
    r << "Host: process-engine \r\n";
    r << "\r\n";

    const auto request = r.str();
    const auto result = client.send(std::span<const unsigned char>(reinterpret_cast<const unsigned char *>(request.data()), request.size()));
  }

  void send_post_request(Client &client, std::string_view path) noexcept {
    std::stringstream r{};
    r << "POST " << path << " HTTP/1.1\r\n";
    r << "Host: process-engine \r\n";
    r << "\r\n";

    const auto request = r.str();
    const auto result = client.send(std::span<const unsigned char>(reinterpret_cast<const unsigned char *>(request.data()), request.size()));
  }

  void send_patch_request(Client &client, std::string_view path, std::string_view json) noexcept {
    std::stringstream r{};
    r << "PATCH " << path << " HTTP/1.1\r\n";
    r << "Host: process-engine \r\n";
    r << "\r\n";
    r << json;

    const auto request = r.str();
    const auto result = client.send(std::span<const unsigned char>(reinterpret_cast<const unsigned char *>(request.data()), request.size()));
  }
} // namespace process_engine::ipc