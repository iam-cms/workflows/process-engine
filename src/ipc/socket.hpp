#pragma once

#ifndef WIN32
#else
#define NOMINMAX
#include <winsock2.h>
#pragma comment(lib, "Ws2_32.lib")
#endif

#include <chrono>
#include <filesystem>
#include <optional>
#include <span>
#include <vector>

namespace process_engine::ipc {
  class Socket final {
  public:
#ifndef _WIN32
    using socket_type = int;
#else
    using socket_type = SOCKET;
#endif

  public:
    Socket() = default;
    Socket(socket_type handle) noexcept;
    Socket(const Socket &) = delete;
    Socket(Socket &&other) noexcept;
    ~Socket();

    Socket &operator=(const Socket &) = delete;
    Socket &operator=(Socket &&other) noexcept;

    [[nodiscard]] socket_type handle() const noexcept;

    [[nodiscard]] bool valid() const noexcept;
    [[nodiscard]] bool block() noexcept;
    [[nodiscard]] bool unblock() noexcept;
    [[nodiscard]] bool blocks() const noexcept;
    [[nodiscard]] bool timeout_after(std::chrono::milliseconds ms) noexcept;

    void shutdown() noexcept;

  private:
    socket_type handle_{};
  };
} // namespace process_engine::ipc

namespace process_engine::ipc {
  [[nodiscard]] std::optional<Socket> create_socket() noexcept;
  [[nodiscard]] std::optional<Socket> create_server_socket(const std::filesystem::path &path) noexcept;

  [[nodiscard]] bool remove_domain_socket(const std::filesystem::path &path) noexcept;

  [[nodiscard]] std::vector<Socket> accept_clients(Socket &socket) noexcept;
  [[nodiscard]] bool connect_to_server(Socket &socket, const std::filesystem::path &path) noexcept;

  [[nodiscard]] std::int64_t read(Socket &socket, std::span<char> buffer) noexcept;
  [[nodiscard]] std::int64_t read(Socket &socket, std::span<unsigned char> buffer) noexcept;

  [[nodiscard]] std::int64_t write(const Socket &socket, std::span<const char> buffer) noexcept;
  [[nodiscard]] std::int64_t write(const Socket &socket, std::span<const unsigned char> buffer) noexcept;
} // namespace process_engine::ipc