#include "socket.hpp"

#ifndef _WIN32

#include <fcntl.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#define INVALID_SOCKET (-1)
#define CLOSE_SOCKET ::close

#else

#include <afunix.h>

#define SHUT_RDWR SD_BOTH
#define CLOSE_SOCKET closesocket
typedef int socklen_t;

#endif

#include <algorithm>
#include <utility>

namespace process_engine::ipc {

  Socket::Socket(socket_type handle) noexcept
      : handle_{handle} {
  }

  Socket::Socket(Socket &&other) noexcept {
    *this = std::move(other);
  }

  Socket::~Socket() {
    if (handle_ != INVALID_SOCKET) {
      CLOSE_SOCKET(handle_);
    }

    handle_ = INVALID_SOCKET;
  }

  Socket &Socket::operator=(Socket &&other) noexcept {
    handle_ = std::exchange(other.handle_, INVALID_SOCKET);
    return *this;
  }

  Socket::socket_type Socket::handle() const noexcept {
    return handle_;
  }

  bool Socket::valid() const noexcept {
    return handle_ != INVALID_SOCKET;
  }

  bool Socket::block() noexcept {
#ifndef _WIN32
    int flags = fcntl(handle_, F_GETFL, 0);
    if (flags == -1) {
      return false;
    }

    return fcntl(handle_, F_SETFL, flags & ~O_NONBLOCK) == 0;
#else
    // 0 for blocking mode.
    u_long mode = 0;
    return ioctlsocket(handle_, FIONBIO, &mode) == 0;
#endif
  }

  bool Socket::unblock() noexcept {
#ifndef _WIN32
    int flags = fcntl(handle_, F_GETFL, 0);
    if (flags == -1) {
      return false;
    }

    return fcntl(handle_, F_SETFL, flags | O_NONBLOCK) == 0;
#else
    // 1 for non-blocking-mode.
    u_long mode = 1;
    return ioctlsocket(handle_, FIONBIO, &mode) == 0;
#endif
  }

  bool Socket::blocks() const noexcept {
#ifndef WIN32
    int flags = fcntl(handle_, F_GETFL, 0);
    return flags == -1 || !(flags & O_NONBLOCK);
#else
    // TODO: Check if correct.
    u_long non_blocking = 0;
    if (ioctlsocket(handle_, FIONBIO, &non_blocking) == SOCKET_ERROR) {
      return true;
    }
    return non_blocking == 0;
#endif
  }

  bool Socket::timeout_after(std::chrono::milliseconds ms) noexcept {
#ifndef _WIN32
    // TODO: Use correct timeout value.
    timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    return setsockopt(handle_, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const char *>(&tv), sizeof(tv)) == 0;
#else
    // TODO: Check if the timeout is correct.
    DWORD to = static_cast<DWORD>(ms.count());
    return setsockopt(handle_, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const char *>(&to), sizeof(to)) == 0;
#endif

    return false;
  }

  void Socket::shutdown() noexcept {
    ::shutdown(handle_, SHUT_RDWR);
    CLOSE_SOCKET(handle_);
    handle_ = INVALID_SOCKET;
  }
} // namespace process_engine::ipc

namespace process_engine::ipc {
  static sockaddr_un create_sockaddr(const std::filesystem::path &path) noexcept {
    const auto tmp_path = path.string();

    sockaddr_un addr{};
    addr.sun_family = AF_UNIX;

    const auto len = std::min(tmp_path.size(), static_cast<size_t>(sizeof(addr.sun_path) - 1));
    std::copy_n(tmp_path.begin(), len, addr.sun_path);

    return addr;
  }

  std::optional<Socket> create_socket() noexcept {

    Socket::socket_type handle{};

    if ((handle = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
      return std::nullopt;
    }

    return Socket(handle);
  }

  std::optional<Socket> create_server_socket(const std::filesystem::path &path) noexcept {
    std::error_code error_code{};
    std::filesystem::remove(path, error_code);

    auto socket = create_socket();
    if (!socket) {
      return std::nullopt;
    }

    if (!socket->unblock()) {
      return std::nullopt;
    }

    const auto addr = create_sockaddr(path);

    if (bind(socket->handle(), reinterpret_cast<const sockaddr *>(&addr), sizeof(addr)) == -1) {
      return std::nullopt;
    }

    // Under Windows, the domain sockets are not real files and therefore the permission cannot be set.
#ifndef WIN32
    std::filesystem::permissions(path, std::filesystem::perms::owner_all, error_code);
    if (error_code) {
      return std::nullopt;
    }
#endif

    if (::listen(socket->handle(), 64) != 0) {
      return std::nullopt;
    }

    return socket;
  }

  bool remove_domain_socket(const std::filesystem::path &path) noexcept {
    std::error_code error_code{};
    return std::filesystem::remove(path, error_code);
  }

  std::vector<Socket> accept_clients(Socket &socket) noexcept {
    std::vector<Socket> clients;

    while (true) {
      Socket::socket_type accepted_socket = ::accept(socket.handle(), nullptr, nullptr);
      if (accepted_socket == INVALID_SOCKET) {
        break;
      }

      clients.emplace_back(accepted_socket);
    }

    return clients;
  }

  bool connect_to_server(Socket &socket, const std::filesystem::path &path) noexcept {
    const auto path_str = path.string();

    sockaddr_un addr{};
    addr.sun_family = AF_UNIX;

    std::copy(path_str.begin(), path_str.end(), addr.sun_path);

    const auto addr_len = path_str.size() + sizeof(addr.sun_family);

    return connect(socket.handle(), reinterpret_cast<const sockaddr *>(&addr), static_cast<socklen_t>(addr_len)) == 0;
  }

  std::int64_t read(Socket &socket, std::span<char> buffer) noexcept {
    if (!socket.valid()) {
      return -1;
    }


#ifndef WIN32
    int flags{MSG_NOSIGNAL};
#else
    int flags{0};
#endif
    return recv(socket.handle(), buffer.data(), static_cast<int>(buffer.size_bytes()), flags);
  }

  std::int64_t read(Socket &socket, std::span<unsigned char> buffer) noexcept {
    return read(socket, std::span<char>(reinterpret_cast<char *>(buffer.data()), buffer.size_bytes()));
  }

  std::int64_t write(const Socket &socket, std::span<const char> buffer) noexcept {
    if (!socket.valid()) {
      return -1;
    }

#ifndef _WIN32
    return send(socket.handle(), buffer.data(), buffer.size_bytes(), MSG_NOSIGNAL);
#else
    return send(socket.handle(), reinterpret_cast<const char *>(buffer.data()), static_cast<int>(buffer.size_bytes()), 0);
#endif
  }

  std::int64_t write(const Socket &socket, std::span<const unsigned char> buffer) noexcept {
    return write(socket, std::span<const char>(reinterpret_cast<const char *>(buffer.data()), buffer.size_bytes()));
  }
} // namespace process_engine::ipc