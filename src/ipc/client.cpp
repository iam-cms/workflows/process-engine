#include "client.hpp"

// TODO: Same as in server.cpp
#ifndef _WIN32

#include <sys/poll.h>
#define POLL poll

#else

#include <winsock2.h>
#define POLL WSAPoll

#endif

#include <array>

namespace process_engine::ipc {
  Client::~Client() {
    disconnect();
  }

  bool Client::connect(const std::filesystem::path &path) noexcept {
    auto result = create_socket();
    if (!result) {
      return false;
    }

    socket_ = std::move(result.value());
    // TODO: Use non-blocking.
    if (!socket_.block()) {
      return false;
    }

    if (!socket_.timeout_after(std::chrono::milliseconds(1000))) {
      return false;
    }

    return connect_to_server(socket_, path);
  }

  void Client::disconnect() noexcept {
    socket_.shutdown();
  }

  std::optional<std::vector<unsigned char>> Client::send(std::span<const unsigned char> buffer) noexcept {
    size_t bytes_written{};

    while (bytes_written < buffer.size_bytes()) {
      const auto write_result = write(socket_, buffer.subspan(bytes_written));

      if (write_result > 0) {
        bytes_written += write_result;
      } else {
        return std::nullopt;
      }
    }

    std::vector<unsigned char> result_buffer{};
    std::array<unsigned char, 1024> input_buffer{};

    while (true) {
      // TODO: The socket is blocking so it returns only after the timeout happens.
      const auto bytes_read = read(socket_, input_buffer);
      // TODO: Check correct error.
      if (bytes_read <= 0) {
        break;
      }

      result_buffer.insert(result_buffer.end(), &input_buffer[0], &input_buffer[bytes_read]);
    }

    return result_buffer;

    // TODO: Receive and write at the same time in non-blocking mode.
    // std::vector<pollfd> polled_fds{};
    // polled_fds.emplace_back(socket_.handle(), POLLIN | POLLOUT, 0);

    // size_t last_bytes_written{};
    // const auto write_result = write(socket_, buffer);
    // if (write_result > 0) {
    //   buffer = buffer.subspan(write_result);
    // }

    // while (true) {
    //   if (POLL(polled_fds.data(), polled_fds.size(), -1) == -1) {
    //     break;
    //   }

    //   if (!socket_.valid()) {
    //     break;
    //   }

    //   const auto &poll_result = polled_fds.at(0);

    //   if (poll_result.revents & POLLIN) {
    //     // const auto bytes_read = read(client, read_buffer);
    //     // if (bytes_read > 0) {
    //     //   on_data_received_(client, std::span<const char>(read_buffer).subspan(0, bytes_read));
    //     //   process_output(client);
    //     // } else {
    //     //   disconnected_clients.emplace_back(client);
    //     // }
    //   } else if (poll_result.revents & POLLOUT) {
    //     // TODO: Write again.
    //     // process_output(client);
    //   } else if (poll_result.revents & (POLLERR | POLLHUP)) {
    //     [[maybe_unused]] const auto result = disconnect();
    //     return std::nullopt;
    //   }
    // }
  }
} // namespace process_engine::ipc