#include "content.hpp"

#include "utility.hpp"

namespace {
  [[nodiscard]] bool is_subdir(const std::filesystem::path &path, const std::filesystem::path &base) noexcept {
    return path.string().starts_with(base.string());
  }
} // namespace

namespace process_engine::ipc {
  std::optional<HttpRoutes> create_static_content_routes(const std::filesystem::path &base_dir) noexcept {
    std::error_code error_code{};
    const auto content_dir = std::filesystem::canonical(base_dir, error_code);

    if (error_code) {
      return std::nullopt;
    }

    const auto get_file = [content_dir](const ipc::HttpRequest &request) {
      std::error_code error_code{};
      const auto path = std::filesystem::canonical(content_dir / request.route_params.at(0), error_code);

      if (error_code || !is_subdir(path, content_dir)) {
        return create_http_response(404);
      }

      const auto content_type = [](const std::filesystem::path &path) {
        if (path.extension() == ".css") {
          return "text/css";
        } else if (path.extension() == ".js") {
          return "text/javascript";
        }

        return "text/html";
      };

      return create_http_response(200, utility::read_file_as_string(path).value(), content_type(path));
    };

    return HttpRoutes{
      {
        "GET",
        HttpRoute{
          .regex = std::regex("/"),
          .handler = [content_dir](const HttpRequest &request) {
            const auto path = content_dir / "index.html";
            if (!std::filesystem::exists(path)) {
              return create_http_response(404);
            }

            return create_http_response(200, utility::read_file_as_string(path).value(), "text/html");
          },
        },
      },
      {
        "GET",
        HttpRoute{
          .regex = std::regex("/static/(.+)"),
          .handler = get_file,
        },
      },
    };
  }
} // namespace process_engine::ipc