#include "server.hpp"

#ifndef _WIN32

#include <sys/poll.h>
#define POLL poll

#else

#include <winsock2.h>
#define POLL WSAPoll

#endif

#include <iostream>
#include <unordered_map>

namespace process_engine::ipc {
  Server::Server(std::filesystem::path socket_path) noexcept
      : socket_path_{std::move(socket_path)} {
  }

  Server::~Server() {
    if (std::filesystem::exists(socket_path_)) {
      std::error_code error{};
      std::filesystem::remove(socket_path_, error);
    }
  }

  bool Server::start(std::stop_token stop_token) noexcept {
    auto server_socket = create_server_socket(socket_path_);
    if (!server_socket) {
      return false;
    }

    socket_ = std::move(server_socket.value());

    work(stop_token);

    socket_.shutdown();

    return true;
  }

  void Server::stop() noexcept {
    socket_.shutdown();
  }

  void Server::work(std::stop_token stop_token) noexcept {
    static constexpr size_t BYTES_PER_READ{1024 * 1024};

    std::vector<char> read_buffer{};
    read_buffer.resize(BYTES_PER_READ);

    std::vector<pollfd> polled_fds{};
    std::vector<Socket> client_sockets{};

    std::vector<Socket> accepted_clients{};
    std::vector<std::reference_wrapper<Socket>> disconnected_clients{};

    polled_fds.emplace_back(socket_.handle(), POLLIN | POLLOUT, 0);
    client_sockets.emplace_back();

    while (!stop_token.stop_requested()) {
      if (POLL(polled_fds.data(), polled_fds.size(), -1) == -1) {
        break;
      }

      if (!socket_.valid()) {
        break;
      }

      for (size_t i = 0; i < polled_fds.size(); ++i) {
        // TODO: polled_fds and client_sockets do often not match.
        const auto &poll_result = polled_fds.at(i);
        auto &client = client_sockets.at(i);

        // Socket has no relevant events.
        if (poll_result.revents == 0) {
          continue;
        }

        // Server.
        if (poll_result.fd == socket_.handle() && poll_result.revents & POLLIN) {
          accepted_clients = accept_clients(socket_);
        }
        // Client.
        else {
          if (poll_result.revents & POLLIN) {
            const auto bytes_read = read(client, read_buffer);
            if (bytes_read > 0) {
              on_data_received_(client, std::span<const char>(read_buffer).subspan(0, bytes_read));
              process_output(client);
            } else {
              disconnected_clients.emplace_back(client);
            }
          } else if (poll_result.revents & POLLOUT) {
            process_output(client);
          } else if (poll_result.revents & (POLLERR | POLLHUP)) {
            disconnected_clients.emplace_back(client);
          }
        }
      }

      for (auto &client : accepted_clients) {
        polled_fds.emplace_back(client.handle(), POLLIN | POLLOUT, 0);
        client_sockets.push_back(std::move(client));
      }

      for (auto &client : disconnected_clients) {
        std::erase_if(polled_fds, [&client](const auto &item) {
          return item.fd == client.get().handle();
        });

        std::erase_if(client_sockets, [&client](const auto &item) {
          return item.handle() == client.get().handle();
        });
      }

      accepted_clients.clear();
      disconnected_clients.clear();
    }

    if (!remove_domain_socket(socket_path_)) {
      // TODO: Log error?
    }
  }

  void Server::process_output(const Socket &client) noexcept {
    if (!outputs_.contains(client.handle())) {
      return;
    }

    auto &output = outputs_.at(client.handle());
    if (output.empty()) {
      return;
    }

    while (!output.empty()) {
      auto stream = output.front();

      const auto bytes_send = write(client, std::span<const char>(stream.buffer).subspan(stream.bytes_send));
      if (bytes_send <= 0) {
        // TODO: Error.
      }

      stream.bytes_send += bytes_send;
      if (stream.bytes_send >= stream.buffer.size()) {
        output.pop();
      }
    }

    if (output.empty()) {
      outputs_.erase(client.handle());
    }
  }

  void Server::write_to_client(const Socket &client, std::string content) noexcept {
    if (!outputs_.contains(client.handle())) {
      outputs_.insert({client.handle(), {}});
    }

    outputs_.at(client.handle()).push({std::move(content), 0});
  }
} // namespace process_engine::ipc