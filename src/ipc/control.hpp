#pragma once

#include "ipc/http.hpp"
#include "workflow/workflow.hpp"

namespace process_engine::ipc {
    [[nodiscard]] HttpRoutes create_workflow_control_routes(workflow::Workflow &workflow, execution::Control &control) noexcept;
}