#include "control.hpp"

namespace process_engine::execution {
  void Control::resume() noexcept {
    continue_signal.notify_all();
  }

  void Control::stop() noexcept {
    stop_source.request_stop();
    continue_signal.notify_all();
  }

  void Control::cancel() noexcept {
    cancel_source.request_stop();
    continue_signal.notify_all();
  }

  bool Control::is_stopped_or_cancelled() noexcept {
    return stop_source.stop_requested() || cancel_source.stop_requested();
  }
} // namespace process_engine::execution