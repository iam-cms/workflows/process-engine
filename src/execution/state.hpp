#pragma once

#include <nlohmann/json.hpp>

namespace process_engine::execution {
  enum class State {
    pending,
    running,
    needs_interaction,
    repeat,
    error,
    finished,
    stopped,
    cancelled,
  };

  constexpr bool is_running_state(const State &state) noexcept {
    return state == State::running || state == State::needs_interaction || state == State::repeat;
  }

  constexpr bool is_error_state(const State &state) noexcept {
    return state == State::error;
  }

  constexpr bool is_exit_state(const State &state) noexcept {
    return state == State::finished || state == State::stopped || state == State::cancelled;
  }

  NLOHMANN_JSON_SERIALIZE_ENUM(State, {
                                        {State::pending, "pending"},
                                        {State::running, "running"},
                                        {State::needs_interaction, "needs_interaction"},
                                        {State::repeat, "repeat"},
                                        {State::error, "error"},
                                        {State::finished, "finished"},
                                        {State::stopped, "stopped"},
                                        {State::cancelled, "cancelled"},
                                      })
} // namespace process_engine::execution