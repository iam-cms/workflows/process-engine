#pragma once

#include "workflow/workflow.hpp"

namespace process_engine::execution {
  void decorate_execution(const workflow::Nodes &nodes) noexcept;
  void decorate_branch_select(const workflow::Nodes &nodes) noexcept;
}