#pragma once

#include <condition_variable>
#include <stop_token>

namespace process_engine::execution {
  struct Control final {
    std::condition_variable continue_signal{};
    std::stop_source stop_source{};
    std::stop_source cancel_source{};

    void resume() noexcept;
    void stop() noexcept;
    void cancel() noexcept;

    [[nodiscard]] bool is_stopped_or_cancelled() noexcept;
  };
}