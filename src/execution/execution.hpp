#pragma once

#include "workflow/workflow.hpp"

namespace process_engine::execution {
  void clear_needs_interaction_states(const workflow::Nodes &nodes) noexcept;
  [[nodiscard]] bool prepare_execution(workflow::Workflow &workflow, const std::filesystem::path &working_dir) noexcept;

  [[nodiscard]] bool resolve_variables(std::string &value, const std::unordered_map<std::string, std::string> &variables, size_t depth = 10) noexcept;
  [[nodiscard]] bool collect_inputs(const workflow::Graph::value_type &graph_entry, const std::unordered_map<std::string, std::string> &variables) noexcept;

  [[nodiscard]] bool has_finished(const workflow::Nodes &nodes) noexcept;

  [[nodiscard]] bool execute_step(workflow::Workflow &workflow, Context &ctx) noexcept;
  [[nodiscard]] bool execute(workflow::Workflow &workflow, Context &ctx) noexcept;
} // namespace process_engine::execution