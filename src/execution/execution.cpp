#include "execution.hpp"

#include "execution/decorator.hpp"
#include "execution/io.hpp"
#include "logging.hpp"
#include "workflow/compatibility.hpp"
#include "workflow/io.hpp"

#include <queue>

using namespace process_engine::node;
using namespace process_engine::workflow;

namespace {
  using namespace process_engine;
  using namespace process_engine::execution;

  void distribute_tokens(const Graph &graph) noexcept {
    for (const auto &[node, ports] : graph) {
      for (auto &[src_port_index, edge] : ports.outgoing) {
        ++edge.node->token;
      }
    }
  }

  bool can_node_be_executed(const Node *node) noexcept {
    return node->state == State::pending && node->token <= 0;
  }

  Node *find_executable_node(const Graph &graph) noexcept {
    auto find_iter = std::find_if(graph.cbegin(), graph.cend(), [](const auto &pair) {
      return can_node_be_executed(pair.first);
    });

    // TODO: Possible improvement:
    // Check only for interactions along a repeated node.
    // This way a loop can be repeated when a interaction outside the loop is needed.
    auto any_interactions = std::any_of(graph.cbegin(), graph.cend(), [](const auto &pair) {
      const auto node = pair.first;
      return node->state == State::needs_interaction;
    });

    // If no node is found and no interaction is needed then try to find a node that wants to be repeated.
    if (find_iter == graph.cend() && !any_interactions) {
      find_iter = std::find_if(graph.cbegin(), graph.cend(), [](const auto &pair) {
        const auto node = pair.first;
        return node->state == State::repeat && node->token <= 0;
      });
    }

    return find_iter == graph.cend() ? nullptr : find_iter->first;
  }

  std::vector<Node *> reset_path(Node *start, const Graph &graph) noexcept {
    // TODO: Collect nodes along path, set their tokens to zero and redistribute the tokens instead of just adding tokens back?
    std::queue<Node *> remaining{};
    remaining.push(start);

    std::vector<Node *> reset_nodes{};

    while (!remaining.empty()) {
      auto node = remaining.front();
      remaining.pop();

      ++node->token;
      reset_nodes.emplace_back(node);

      // Reset output ports.
      for (auto &port : node->outputs) {
        port.value = {};
      }

      node->state = State::pending;

      // Reset the next nodes.
      for (auto &output : graph.at(node).outgoing) {
        remaining.push(output.second.node);
      }
    }

    return reset_nodes;
  }

  void prepare_repeat(Workflow &workflow, Node *node, Context &ctx) noexcept {
    const auto branches = node->select_branches(node, ctx);
    const auto &ports = workflow.graph.at(node);

    for (const auto branch : branches) {
      const auto range = ports.outgoing.equal_range(branch);

      // Reset the selected branches.
      for (auto range_iter = range.first; range_iter != range.second; ++range_iter) {
        // Remove the resetted nodes from the finished statistics.
        workflow.nodes_finished -= reset_path(range_iter->second.node, workflow.graph).size();
      }
    }

    // Add tokens for the "data" ports.
    for (auto &connection : ports.outgoing) {
      auto &type = node->outputs.at(connection.first).type;
      if (type != type_to_string<Dependency>()) {
        ++connection.second.node->token;
      }
    }
  }

  void state_from_control(Workflow &workflow, const Control &control) noexcept {
    if (control.stop_source.stop_requested()) {
      workflow.state = State::stopped;
    } else if (control.cancel_source.stop_requested()) {
      workflow.state = State::cancelled;
    }
  }

  bool execute_node(const Graph::value_type &graph_entry, Context &ctx) noexcept {
    const auto &[node, ports] = graph_entry;

    node->state = State::running;

    // Get the inputs by fetching the outputs of the previous nodes.
    if (!collect_inputs(graph_entry, ctx.variables)) {
      return false;
    }

    try {
      node->state = node->skip ? State::finished : node->execute(node, ctx);
    } catch (const std::exception &e) {
      node->state = State::error;
      LOG_EXECUTION_ERROR(ctx.logger, node->id, e.what());
    }

    if (node->state == State::error) {
      return false;
    }

    if (node->type != Node::Type::control && node->state == State::finished) {
      // When the node has finished, reduce the tokens of the connected nodes.
      for (auto &[port_index, edge] : ports.outgoing) {
        --edge.node->token;
      }
    } else if (node->type == Node::Type::control) {
      // Select the branch and reduce the tokens of the nodes connected to the branch.
      for (const auto branch : node->select_branches(node, ctx)) {
        const auto range = ports.outgoing.equal_range(branch);
        for (auto range_iter = range.first; range_iter != range.second; ++range_iter) {
          --range_iter->second.node->token;
        }
      }

      // Reduce the tokens for the "data" ports.
      for (auto &[port_index, edge] : ports.outgoing) {
        if (node->outputs.at(port_index).type != type_to_string<Dependency>()) {
          --edge.node->token;
        }
      }
    }

    return true;
  }

  [[nodiscard]] bool handle_possible_exit(Workflow &workflow, Context &ctx) noexcept {
    if (has_finished(workflow.nodes)) {
      workflow.finished_now();
      LOG_ENGINE_EXECUTION_INFO(ctx.logger, "Execution finished");

      return true;
    } else if (ctx.control.stop_source.stop_requested()) {
      workflow.state = State::stopped;
      LOG_ENGINE_EXECUTION_INFO(ctx.logger, "Execution stopped");

      return true;
    } else if (ctx.control.cancel_source.stop_requested()) {
      workflow.state = State::cancelled;
      LOG_ENGINE_EXECUTION_INFO(ctx.logger, "Execution cancelled");

      return true;
    } else if (ctx.exit_when_no_work || is_error_state(workflow.state) || is_exit_state(workflow.state)) {
      return true;
    }

    return false;
  }
} // namespace

namespace process_engine::execution {

  void clear_needs_interaction_states(const Nodes &nodes) noexcept {
    for (auto &node : nodes) {
      if (node->state == State::needs_interaction) {
        node->state = State::pending;
      }
    }
  }

  bool prepare_execution(Workflow &workflow, const std::filesystem::path &working_dir) noexcept {
#ifndef NDEBUG
    decorate_execution(workflow.nodes);
    decorate_branch_select(workflow.nodes);
#endif

    if (!can_restore_state_from_file(working_dir) || !restore_state_from_file(workflow, working_dir)) {
      distribute_tokens(workflow.graph);
    }

    clear_needs_interaction_states(workflow.nodes);

    return true;
  }

  bool resolve_variables(std::string &value, const std::unordered_map<std::string, std::string> &variables, size_t depth) noexcept {
    static const std::regex variable_regex{R"((\$\{(.*?)\}))"};

    for (size_t i = 0; i < depth; ++i) {
      auto begin = std::sregex_iterator(value.begin(), value.end(), variable_regex);
      auto end = std::sregex_iterator();

      if (begin == end) {
        break;
      }

      std::vector reverse(begin, end);
      for (auto it = reverse.rbegin(); it != reverse.rend(); ++it) {
        const auto &match = *it;

        const auto &variable_name = match[2].str();
        if (!variables.contains(variable_name)) {
          // TODO: Add interaction?
          return false;
        }

        value.replace(match.position(1), match.length(1), variables.at(variable_name));
      }
    }

    return true;
  }

  bool collect_inputs(const Graph::value_type &graph_entry, const std::unordered_map<std::string, std::string> &variables) noexcept {
    const auto &[node, ports] = graph_entry;

    for (const auto &[dst_port_index, edge] : ports.incoming) {

      auto &output_port = edge.node->outputs.at(edge.port_index);
      auto &input_port = node->inputs.at(dst_port_index);

      // TODO: This does a copy. Maybe use references or pointers for the inputs.
      if (input_port.type != type_to_string<Dependency>()) {
        std::string value = output_port.value;

        if (output_port.name != StdoutPort::name) {
          if (!resolve_variables(value, variables)) {
            return false;
          }
        }

        input_port.value = std::move(value);
      }
    }

    return true;
  }

  bool has_finished(const Nodes &nodes) noexcept {
    if (nodes.empty()) {
      return true;
    }

    const auto all_pending = std::all_of(nodes.cbegin(), nodes.cend(), [](const auto &node) { return node->state == State::pending; });

    if (all_pending) {
      return false;
    }

    return !std::any_of(nodes.cbegin(), nodes.cend(), [](const auto &node) { return is_running_state(node->state) || is_error_state(node->state); });
  }

  bool execute_step(Workflow &workflow, Context &ctx) noexcept {
    if (ctx.control.is_stopped_or_cancelled()) {
      state_from_control(workflow, ctx.control);
      return false;
    }

    auto node = find_executable_node(workflow.graph);
    if (node == nullptr) {
      return false;
    }

    if (node->state == State::repeat) {
      prepare_repeat(workflow, node, ctx);
    }

    // TODO: Setting the state and saving the state here is only done for compatibility reason.
    node->state = State::running;
    if (!store_state_to_file(workflow, ctx.working_dir)) {
      LOG_ENGINE_EXECUTION_ERROR(ctx.logger, "Could not save state");
    }

    bool result{true};

    if (!execute_node(*workflow.graph.find(node), ctx)) {
      workflow.state = State::error;
      result = false;
    } else {
      if (node->state == State::needs_interaction) {
        workflow.state = State::needs_interaction;
      } else if (node->state == State::finished) {
        ++workflow.nodes_finished;
      }
    }

    if (!store_state_to_file(workflow, ctx.working_dir)) {
      LOG_ENGINE_EXECUTION_ERROR(ctx.logger, "Could not save state");
    }

    return result;
  }

  bool execute(Workflow &workflow, Context &ctx) noexcept {
    LOG_ENGINE_EXECUTION_INFO(ctx.logger, "Execution started");

    workflow.variables["TMP_FOLDER"] = workflow::tmp_dir(ctx.working_dir).string();

    workflow.started_now();

    std::mutex run_mutex{};

    while (!ctx.control.is_stopped_or_cancelled()) {
      while (execute_step(workflow, ctx));

      if (handle_possible_exit(workflow, ctx)) {
        break;
      } else {
        // TODO: Timeout after which the process exits to avoid zombie processes?
        // TODO: Does this work with subworkflows?
        std::unique_lock lock{run_mutex};
        ctx.control.continue_signal.wait(lock);

        // Restore interactions in the case the file is set manually.
        if (auto loaded_interactions = workflow::load_interactions_from_file(ctx.working_dir)) {
          workflow.interactions = std::move(loaded_interactions.value());
        }
      }
    }

    state_from_control(workflow, ctx.control);
    if (!store_state_to_file(workflow, ctx.working_dir)) {
      // TODO: Allowed?
    }

    return true;
  }
} // namespace process_engine::execution