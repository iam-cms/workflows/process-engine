#pragma once

#include "execution/control.hpp"
#include "execution/state.hpp"
#include "logging.hpp"
#include "workflow/interaction.hpp"
#include "workflow/variable.hpp"

#include <nlohmann/json.hpp>

#include <filesystem>

namespace process_engine::execution {
  struct Context final {
    Control &control;
    workflow::Interactions &interactions;
    workflow::Variables &variables;
    std::vector<std::filesystem::path> &artifacts;
    std::filesystem::path working_dir{};
    bool exit_when_no_work{};

    std::shared_ptr<spdlog::logger> logger{};
  };
} // namespace process_engine::execution