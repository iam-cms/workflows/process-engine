#include "decorator.hpp"

#include "node/source.hpp"

#include <sstream>

using namespace process_engine::node;

namespace process_engine::execution {
  class LogStateChange final {
  public:
    LogStateChange(Node::Executor &&execute) noexcept
        : execute_{execute} {}

    State operator()(Node *node, Context &ctx) {
      const auto log_state_transition = [&node, &ctx](execution::State old_state, execution::State new_state) {
        std::stringstream state_transition{};
        state_transition << "changed from " << nlohmann::json(old_state) << " to " << nlohmann::json(new_state);

        LOG_EXECUTION_INFO(ctx.logger, node->id, state_transition.str());
      };

      log_state_transition(execution::State::pending, execution::State::running);

      const auto old_state = node->state;
      const auto new_state = execute_(node, ctx);

      log_state_transition(old_state, new_state);

      return new_state;
    };

  private:
    Node::Executor execute_{};
  };

  class LogBranchSelect final {
  public:
    LogBranchSelect(Node::BranchSelect &&select) noexcept
        : select_{select} {}

    std::vector<int> operator()(Node *node, execution::Context &ctx) {
      const auto selected_branches = select_(node, ctx);

      std::stringstream msg_stream{};

      msg_stream << "selects [";

      for (const auto branch : selected_branches) {
        msg_stream << node->outputs.at(branch).name << ", ";
      }

      msg_stream << "]";

      LOG_EXECUTION_INFO(ctx.logger, node->id, msg_stream.str());

      return selected_branches;
    };

  private:
    Node::BranchSelect select_{};
  };

  class LogStatistics final {
  public:
    LogStatistics(Node::Executor &&execute) noexcept
        : execute_{execute} {}

    State operator()(Node *node, Context &ctx) {
      const auto start = std::chrono::high_resolution_clock::now();
      const auto state = execute_(node, ctx);
      const std::chrono::duration<float> elapsed = std::chrono::high_resolution_clock::now() - start;

      std::stringstream msg{};
      msg << "took " << std::fixed << elapsed.count() << "s to complete";
      LOG_EXECUTION_INFO(ctx.logger, node->id, msg.str());

      return state;
    };

  private:
    Node::Executor execute_{};
  };

  void decorate_execution(const workflow::Nodes &nodes) noexcept {
    for (auto &node : nodes) {
      if (node::is_source_node(node->name)) {
        continue;
      }

      node->execute = LogStatistics(std::move(node->execute));
      node->execute = LogStateChange(std::move(node->execute));
    }
  }

  void decorate_branch_select(const workflow::Nodes &nodes) noexcept {
    for (auto &node : nodes) {
      node->select_branches = LogBranchSelect(std::move(node->select_branches));
    }
  }
} // namespace process_engine::execution