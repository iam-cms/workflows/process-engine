#pragma once

#include "workflow/workflow.hpp"

namespace process_engine::execution {
  [[nodiscard]] bool store_state_to_file(const workflow::Workflow &workflow, const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] bool can_restore_state_from_file(const std::filesystem::path &working_dir) noexcept;
  [[nodiscard]] bool restore_state_from_file(workflow::Workflow &workflow, const std::filesystem::path &working_dir) noexcept;
} // namespace process_engine::execution