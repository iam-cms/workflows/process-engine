#include "setup.hpp"

#include "logging.hpp"
#include "workflow/io.hpp"

#include <sstream>

namespace {
  bool create_dir(const std::filesystem::path &path) {
    if (std::filesystem::exists(path)) {
      return true;
    }

    std::error_code error_code{};
    if (!std::filesystem::create_directories(path, error_code)) {
      std::stringstream error{};
      error << "Could not create directory '" << path.string() << "': " << error_code.message();

      LOG_ENGINE_EXECUTION_ERROR(process_engine::logging::execution_logger(), error.str());
      return false;
    }

    return true;
  }
} // namespace

namespace process_engine::execution {
  std::optional<std::filesystem::path> setup_working_dir(const std::filesystem::path &working_dir, const std::filesystem::path &workflow_file) noexcept {
    if (!std::filesystem::exists(workflow_file)) {
      return {};
    }

    if (!create_dir(working_dir) ||
        !create_dir(workflow::log_dir(working_dir)) ||
        !create_dir(workflow::state_dir(working_dir)) ||
        !create_dir(workflow::ipc_dir(working_dir)) ||
        !create_dir(workflow::tmp_dir(working_dir))) {
      return {};
    }

    const auto workflow_file_in_working_dir = working_dir / workflow_file.filename();

    std::error_code error_code{};
    if (!std::filesystem::exists(workflow_file_in_working_dir, error_code) && !std::filesystem::copy_file(workflow_file, workflow_file_in_working_dir, error_code)) {
      std::stringstream error{};
      error << "Could not copy workflow file '" << workflow_file.string() << " into working directory '" << working_dir.string() << "': " << error_code.message();
      LOG_ENGINE_EXECUTION_ERROR(logging::execution_logger(), error.str());

      return {};
    }

    LOG_ENGINE_EXECUTION_INFO(logging::execution_logger(), "Working directory initialized");

    return workflow_file_in_working_dir;
  }
} // namespace process_engine::execution