#pragma once

#include <optional>
#include <filesystem>

namespace process_engine::execution {
  [[nodiscard]] std::optional<std::filesystem::path> setup_working_dir(const std::filesystem::path &working_dir, const std::filesystem::path &workflow_file) noexcept;
} // namespace process_engine::execution