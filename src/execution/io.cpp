#include "io.hpp"

#include "workflow/compatibility.hpp"
#include "workflow/io.hpp"

namespace process_engine::execution {
  bool store_state_to_file(const workflow::Workflow &workflow, const std::filesystem::path &working_dir) noexcept {
    return workflow::store_interactions_to_file(workflow.interactions, working_dir) &&
           workflow::store_node_states_to_file(workflow.nodes, working_dir) &&
           workflow::store_status_to_file(workflow, working_dir) &&
           workflow::store_deprecated_status_to_file(workflow, working_dir) &&
           workflow::store_deprecated_tree_to_file(workflow.graph, working_dir);
  }

  bool can_restore_state_from_file(const std::filesystem::path &working_dir) noexcept {
    std::error_code error{};
    return std::filesystem::exists(workflow::interactions_path(working_dir), error) &&
           std::filesystem::exists(workflow::status_path(working_dir), error) &&
           std::filesystem::exists(workflow::nodes_path(working_dir), error);
  }

  bool restore_state_from_file(workflow::Workflow &workflow, const std::filesystem::path &working_dir) noexcept {
    if (auto restored_interactions = workflow::load_interactions_from_file(working_dir)) {
      workflow.interactions = std::move(restored_interactions.value());
    } else {
      return false;
    }

    if (!workflow::load_status_from_file(workflow, working_dir)) {
      return false;
    }

    return workflow::load_node_states_from_file(workflow.nodes, working_dir);
  }

} // namespace process_engine::execution